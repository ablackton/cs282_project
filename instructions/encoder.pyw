import random, re, os, pickle, subprocess, datetime, difflib, webbrowser
from tkinter import *
from tkinter.filedialog import asksaveasfilename, askdirectory, askopenfilename
from tkinter.messagebox import showerror

class CodeFrame(LabelFrame):
    def __init__(self, master, val=1, txt = 'Codes'):
        LabelFrame.__init__(self, master, text=txt)
        codevar = self.codevar = IntVar()
        codevar.set(val)
        Radiobutton(self, value = 1, text= '1 Byte', variable = codevar).grid(sticky=W)
        Radiobutton(self, value = 2, text= '2 Bytes', variable = codevar).grid(sticky=W)
        Radiobutton(self, value = 3, text= '3 Bytes', variable = codevar).grid(sticky=W)
        Radiobutton(self, value = 4, text= '4 Bytes', variable = codevar).grid(sticky=W)
        Radiobutton(self, value = 5, text= 'All', variable = codevar).grid(sticky=W)
        for r in range(5):
            self.rowconfigure(r,weight=1)
            
    def get(self):
        return self.codevar.get()
        
class FileFrame(LabelFrame):
    def __init__(self, master, txt, base):
        LabelFrame.__init__(self, master, text=txt)
        self.namevar = StringVar()
        self.namevar.set(base)
        self.entry = Entry(self, textvariable=self.namevar)
        button = Button(self, text='Browse...', command=self.setDlg)
        self.entry.grid(row=0, column = 0, sticky = 'ew')
        button.grid(row=0, column=1)        
    def get(self):
        return self.namevar.get()
    def set(self, name):
        self.namevar.set(name)
    def setDlg(self):
        s = asksaveasfilename(title="File Base Name", 
                    filetypes=[("Decoded Files", '.txt'),
                               ("Encoded Files", '.bin'), 
                               ("All Files", '.*')])
        if s:
            fname = os.path.basename(s)
            base = os.path.splitext(fname)[0]
            self.namevar.set(base)
            
class DirectoryFrame(LabelFrame):
    def __init__(self, master, title, dataDir):
        LabelFrame.__init__(self, master, text=title)
        self.namevar = StringVar()
        self.namevar.set(dataDir)
        self.entry = Entry(self, textvariable=self.namevar)
        self.entry.xview_moveto(1)
        button = Button(self, text='Browse...', command=self.set)
        self.entry.grid(row=0, column = 0, sticky = 'ew')
        button.grid(row=0, column=1)
    def get(self):
        return self.namevar.get()
    def set(self):
        s = askdirectory(initialdir=self.get())
        if s:            
            self.namevar.set(os.path.normpath(s))
            self.entry.xview_moveto(1)

class MarsFrame(LabelFrame):
    def __init__(self, master, txt, mars):
        LabelFrame.__init__(self, master, text=txt)
        self.namevar = StringVar()
        self.namevar.set(mars)
        self.entry = Entry(self, textvariable=self.namevar)
        self.entry.xview_moveto(1.0)
        button = Button(self, text='Browse...', command=self.setDlg)
        self.entry.grid(row=0, column = 0, sticky = 'ew')
        button.grid(row=0, column=1)        
    def get(self):
        return self.namevar.get()
    def set(self, name):
        self.namevar.set(name)
    def setDlg(self):
        s = askopenfilename(title="MARS Jar File", 
                    filetypes=[("Jar Files", '.jar')])
        if s:
            self.namevar.set(os.path.normpath(s))
            self.entry.xview_moveto(1.0)
            
class ProgFrame(LabelFrame):
    def __init__(self, master, txt, asm):
        LabelFrame.__init__(self, master, text=txt)
        self.namevar = StringVar()
        self.namevar.set(asm)
        self.entry = Entry(self, textvariable=self.namevar)
        self.entry.xview_moveto(1.0)
        button = Button(self, text='Browse...', command=self.setDlg)
        self.entry.grid(row=0, column = 0, sticky = 'ew')
        button.grid(row=0, column=1)        
    def get(self):
        return self.namevar.get()
    def set(self, name):
        self.namevar.set(name)
    def setDlg(self):
        direct = os.path.dirname(self.get())
        s = askopenfilename(title="Main Program File", 
                    filetypes=[("MIPS Files", '.s'),("MIPS Files", '.asm')],
                    initialdir = direct)
        if s:
            self.namevar.set(os.path.normpath(s))
            self.entry.xview_moveto(1.0)

class SampleFrame(LabelFrame):
    def __init__(self, master, title, size):
        LabelFrame.__init__(self, master, text=title)
        self.sizevar = StringVar()
        self.sizevar.set(size)
        entry = Entry(self, justify=RIGHT, textvariable=self.sizevar)
        entry.grid(sticky='ew')
    def get(self):
        return int(self.sizevar.get())
    
class ScrolledText(Frame):
    #Adapted from "Programming Python"
    
    def __init__(self, parent=None, text='', file=None):
        Frame.__init__(self, parent)
        self.makeWidgets()
        self.setText(text, file)
        self.text.configure(state=DISABLED)
        
    def makeWidgets(self):
        
        sbar = Scrollbar(self)
        text = Text(self, relief=SUNKEN, wrap=WORD, width=65, height = 6)
        sbar.config(command=text.yview)                  # xlink sbar and text
        text.config(yscrollcommand=sbar.set)             # move one moves other
        sbar.pack(side=RIGHT, fill=Y)                    # pack first=clip last
        text.pack(side=LEFT, expand=YES, fill=BOTH)      # text clipped first
        self.text = text

    def setText(self, text='', file=None):
        self.insertText('1.0', text, file)
        
    def addText(self, text='', file=None):
        self.insertText(END, text, file)
        self.text.yview_moveto(1.0)
        
    def insertText(self, where, text='', file=None):
        self.text.configure(state=NORMAL)
        if file:
            text = file.read()
        self.text.insert(where, text)                      # add at end
        self.text.configure(state=DISABLED)

    def getText(self):                                   # returns a string
        return self.text.get('1.0', END+'-1c')           # first through last
        
    
class EncoderApp(Frame):
    def __init__(self, master, parms, *args, **kwargs):
        Frame.__init__(self, master, *args, **kwargs)
        self.makeWidgets(parms)
        
    def makeWidgets(self, parms):
        try:
            codelen = parms['codelen']
        except KeyError:
            codelen = 1
        codes = self.codes = CodeFrame(self, val=codelen)
        codes.grid(row = 0, column=0, rowspan = 5, sticky = 'ns')
        try:
            size =parms['sampleSize']
        except KeyError:
            size =100
        sample = self.sample = SampleFrame(self, 'Sample Size', size=size)
        sample.grid(row=0, column=1, sticky = 'ew',padx=2)
        try:
            dd = parms['dataDir']
        except KeyError:
            dd = os.getcwd()
        dFrame = self.dir = DirectoryFrame(self, 'Data Directory', dd)
        dFrame.grid(row=1, column=1, sticky='ew', padx=2)
        try:
            mars = parms['jar']
        except KeyError:
            mars = ''
        mFrame = self.mars = MarsFrame(self, 'MARS Jar File', mars)
        mFrame.grid(row=2, column=1, sticky='ew',padx=2)
        try:
            prog = parms['program']
        except KeyError:
            prog = ''
        pFrame = self.prog = ProgFrame(self, 'MIPS Program File', prog)
        pFrame.grid(row=3, column=1, sticky='ew',padx=2)
        try:
            base = parms['basename']
        except KeyError:
            base = ''
        bFrame = self.fileBase = FileFrame(self, 'File Base Name', base)
        bFrame.grid(row=4, column=1, sticky='ew',padx=2)
        
        text = self.text = ScrolledText(self)
        text.grid(row=0, column=2, rowspan=5, sticky = 'ns', padx=2)
        
        buttonBar = Frame(self, relief=GROOVE, bd=2)
        buttonBar.grid(row=5, column = 0, columnspan=3, pady=4, sticky = 'ew')
        for c in range(3):
            buttonBar.columnconfigure(c, weight=1)
        
        start = Button(buttonBar, text='New Test', command=self.newTest, width = 9)
        start.grid(row=0, column = 0, padx=5 )
        rerun = Button(buttonBar, text='Re-Run', command=self.test, width = 9)
        rerun.grid(row=0, column = 1, padx = 5)
        helpb = Button(buttonBar, text='Help', command=self.help, width = 9)
        helpb.grid(row=0, column = 2, padx = 5) 
        
    def newTest(self):
        codelen = self.codes.get()
        sampleSize = self.sample.get()
        direct = self.dir.get()
        base = self.fileBase.get()
        if not base:
            showerror('Missing Data', 'File Base Name Not Specified') 
            return
        infile = os.path.join(direct, base+'.txt')
        if codelen == 1:
            pop = range(0, 0x80)
        elif codelen == 2:
            pop = range(0x80, 0x800)
        elif codelen == 3:
            pop = range(0x800, 0x10000)
        elif codelen == 4:
            pop = range(0x10000, 0x110000)
        elif codelen == 5:
            pop = range(0, 0x110000)
               
        if sampleSize > len(pop):
            sample = pop
        else:
            sample = random.sample(pop, sampleSize)
        sample = [s for s in sample if not 0xD800 <= s <= 0xDFFF]
        sample.sort()
        with open(infile, 'w') as fin:
            for s in sample:
                fin.write('U+%04X\n' %s)
        
        self.test()
        
    def test(self):
        fmt = '%a %d %B %Y %I:%M:%S %p'      # timestamp format        
        direct = self.dir.get()
        base = self.fileBase.get()
        infile  = os.path.join(direct, base + '.txt')
        testfile = os.path.join(direct, base + '.bin')
        outfile = os.path.join(direct, base + '_mips.bin')
        htmlfile = os.path.join(direct, base + '.htm')
        jar = self.mars.get()
        prog = self.prog.get()
        
        if not os.path.exists(infile):
            showerror('Missing Input File', os.path.normpath(infile))
            return
        
        self.encode(infile, testfile)
        
        # Run MIPS progam
        
        cmd = ['java', '-jar', jar, 'p', 'sm', prog, 'pa', 'e', infile, outfile]
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)
        proc.wait()
        result = proc.stdout.read()
        self.text.addText(text = result)
        
        copyRight = 'MARS 4.1  Copyright 2003-2011 Pete Sanderson and Kenneth Vollmar\n\n\n'
        if result == copyRight:
            computed = ['%x'%x for x in open(outfile,'rb').read()]
            expected = ['%x'%x for x in open(testfile,'rb').read()]
            
            if computed == expected:
                self.text.addText(text = '%s passed: Computed output is as expected\n' %base)
                
            else:
                self.text.addText(text = '%s failed: See %s\n' %(base, os.path.split(htmlfile)[-1]))
                # Compare to expected output
                
                with open(htmlfile, 'w') as fout:
                    fout.write(difflib.HtmlDiff().make_file(expected,computed,
                                fromdesc=base+'.bin', todesc=base+'_mips.bin'))
                webbrowser.open_new_tab(htmlfile)
            
        timestamp = datetime.datetime.now().strftime(fmt)
        self.text.addText(text=timestamp + '\n\n')
        
    def saveParms(self):
        parms = {}
        parms['codelen'] = self.codes.get()
        parms['sampleSize'] = self.sample.get()
        parms['dataDir'] = self.dir.get()
        parms['jar'] = self.mars.get()
        parms['program'] = self.prog.get()
        parms['basename'] = self.fileBase.get()
        
        with open('encoder.ini', 'wb') as fout:
            pickle.dump(parms, fout)
            
    def rerun(self):
        self.test()
    
    def help(self):
        helpText = '''This app will generate test inputs for your encoder program, and compute the expected outputs.  It will run your MIPS program, and compare the computed output to the expected output.

For each test you run, you will need to supply a base name for the files created.  If the base name is "etest1", say, then the test input file will be named etest1.txt, the expected output file will be named etest1.bin, and the output file computed by your MIPS program will be named etest1_mips.bin.  If the computed output differs from the expected output, an html file displaying the differences will also be created.  In out example, it will be called etest1.htm.  

Finally, a log file, named elog.txt, will be created, containing all output to the console area of the app.  Output from each session is appended to the log.  If you don't want this, you can just delete the log, and a new one will be created.

All these files will be placed in whatever directory you designate as the data directory.

The "New Test" creates a new random input file.  The "Re-Run" button will rerun an old test, using an existing input file.  You just have to place the base name in the appropriate entry box.  '''
        
        win = Toplevel()
        win.title('Encoder Test Help')
        text = Text(win, wrap= WORD)
        text.insert(END, helpText)
        text.configure(state=DISABLED)
        ok = Button(win, text='Okay', command = win.destroy)
        text.pack()
        ok.pack()
    
    def writeLog(self):
        logname = os.path.join(self.dir.get(), 'elog.txt')
        with open(logname, 'a') as fout:
            fout.write(self.text.getText())
            
    def encode(self, infile, outfile):
        pattern = re.compile(r'U\+[A-F0-9]+\n')
        with open(outfile, 'wb') as fout:
            for line in open(infile):
                m = pattern.match(line)
                if not m:
                    raise ValueError('Invalid input %s' % line)
                fout.write(chr(int(m.string[2:-1], 16)).encode())
                
def wrapup():
    app.saveParms()
    app.writeLog()
    root.destroy()
    root.quit()

if __name__ == '__main__':
    random.seed()
    try:
        parms = pickle.load(open('encoder.ini', 'rb'))
    except (IOError, EOFError):
        parms = {}
    root = Tk()
    root.resizable(0,0)
    root.title('UTF-8 Encoding Test')
    app=EncoderApp(root, parms, bd=2, relief=GROOVE)
    app.grid()
    root.wm_protocol("WM_DELETE_WINDOW", wrapup)
    root.mainloop()
