#--------------------------------------------------
# decoder.asm
# Function
# Name: decoder
# ********************
# Register usage:
#	inBuff: 	$s0
#	end of buffer:	$s1
#	outBuff:	$s2 
#	b1:		$t1
#	b2:		$t2
#	b3:		$t3
#	b4:		$t4
# Revision History:
#   2012.03.08	Keith
#--------------------------------------------------
.globl	decoder
.text
		
		
decoder:	
		move	$s0, $a0		# $s0 = inBuff
		move	$s1, $a1		# $s1 = end
		move	$s2, $a2		# $s2 = outBuff

		addiu	$sp, $sp, -4		# allocate word on stack
		sw	$ra, ($sp)		# store return address on stack
		
loop:		beq	$s0, $s1, done		# if (inBuff == end): goto done
		li	$t2, 0			# b2 = 0
		li	$t3, 0			# b3 = 0
		li	$t4, 0			# b4 = 0
		
		lbu	$t1, ($s0)		# b1 = *inBuff
		addiu	$s0, $s0, 1		# inBuff++
		
		ble	$t1, 0x7F, advance	# if (b1 <= 0x7F): goto advance
		lbu	$t2, ($s0)		# b2 = *inBuff
		addiu	$s0, $s0, 1		# inBuff++
		
		ble	$t1, 0xDF, advance	# if (b1 <= 0xDF): goto advance
		lbu	$t3, ($s0)		# b3 = *inBuff
		addiu	$s0, $s0, 1		# inBuff++
		
		ble	$t1, 0xEF, advance	# if (b1 <= 0xEF): goto advance
		lbu	$t4, ($s0)		# b4 = *inBuff
		addiu	$s0, $s0, 1		# inBuff++
		b 	advance			# goto advance
		
advance:			
		move	$a0, $t1		# $a0 = b1
		move	$a1, $t2		# $a1 = b2
		move	$a2, $t3		# $a2 = b3
		move	$a3, $t4		# $a3 = b4
		jal	decode			# function call
		
		move	$a0, $v0		# initialize hexWrite input - hexNum
		move	$a1, $s2		# initialize hexWrite input - &outputBuffer
		jal	hexWrite		# call hexWrite(hexNum, &outputBuffer)
		move	$s2, $v0		# outputBuffer = char* hexWrite(hexNum, &outputBuffer)
		b 	loop			# goto loop
		
done:
		move	$v0, $s2		# return updated outBuff

		lw	$ra, ($sp)		# load return address from stack
		addiu	$sp, $sp, 4		# deallocate word on stack
		jr	$ra			# function return
