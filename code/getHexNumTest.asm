#--------------------------------------------------
# getHexNumTest.asm
# Function
# Name: getHexNumTest
# ********************
#
# Description:
#   Test Functions for getHexNum function
#
# Revision History:
#   2012.02.18	Andrew Blackton <aabxvc@mail.umkc.edu>	Test Bench Creation
#--------------------------------------------------
.data
missingUCodePoint:	.asciiz	"Hello"
missingPCodePoint:	.asciiz	"Uello"
goodZeroCodePoint:	.asciiz "U+0"
goodNineCodePoint:	.asciiz "U+9"
goodACodePoint:		.asciiz	"U+AD"
goodFCodePoint:		.asciiz	"U+FFFF"
returnValue:		.word	0:1
.text

getHexNumTest:
# missingU Expected Result
# :: ---- :: Error :: ---- :: getHexNum :: ---- ::
# Incorrect Unicode Code Point Input Format, missing U
# :: ---- :: Error :: ---- :: getHexNum :: ---- ::
# missingU:
# 	la	$a0, missingUCodePoint		# (a0.1 <- *inputBuffer) initialize *inBuffer
# 	jal	getHexNum
# 
# 	li	$v0, 1				# (v0.0) print int
# 	move	$a0, $v0
# 	syscall					# print null-terminated string

# missingP Expected Result
# :: ---- :: Error :: ---- :: getHexNum :: ---- ::
# Incorrect Unicode Code Point Input Format, missing +
# :: ---- :: Error :: ---- :: getHexNum :: ---- ::
# missingP:
# 	la	$a0, missingPCodePoint		# (a0.1 <- *inputBuffer) initialize *inBuffer
# 	jal	getHexNum
# 
# 	li	$v0, 1				# (v0.0) print int
# 	move	$a0, $v0
# 	syscall					# print null-terminated string

goodZero:
	la	$a0, goodZeroCodePoint		# (a0.1 <- *inputBuffer) initialize *inBuffer
	jal	getHexNum

	move	$a0, $v0
	li	$v0, 1				# (v0.0) print int
	syscall					# print null-terminated string
	
goodNine:
	la	$a0, goodNineCodePoint		# (a0.1 <- *inputBuffer) initialize *inBuffer
	jal	getHexNum

	move	$a0, $v0
	li	$v0, 1				# (v0.0) print int
	syscall					# print null-terminated string

goodA:
	la	$a0, goodACodePoint		# (a0.1 <- *inputBuffer) initialize *inBuffer
	jal	getHexNum

	move	$a0, $v0
	li	$v0, 1				# (v0.0) print int
	syscall					# print null-terminated string

goodF:
	la	$a0, goodFCodePoint		# (a0.1 <- *inputBuffer) initialize *inBuffer
	jal	getHexNum

	move	$a0, $v0
	li	$v0, 1				# (v0.0) print int
	syscall					# print null-terminated string

exit:
	li	$v0, 10			# (v0.0) terminate execution
	syscall				# graceful termination
	
