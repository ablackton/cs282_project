#--------------------------------------------------
# getHexNum.asm
# Function
# Name: getHexNum
# ********************
# int getHexNum(char* inBuffer)
#   if *inBuffer != ‘U’:
#     exit('Expected U’)
#   inBuffer += 1
#   if *inBuffer != ‘+’:
#     exit(“expected +”)
#   codePointTotal = 0
#   inBuffer += 1
#   while *inBuffer != 0:
#     codePointTotal <<= 4 // multiply by 16
#     if ‘0’ < *inBuffer <= ‘9’:
#       codePointTotal += *inBuffer – ‘0’
#     else if ‘A’ <= *inBuffer <= ‘F’:
#       codePointTotal += *inBuffer - ‘A’ + 10
#     else: exit(‘Illegal digit”)
# return codePointTotal
#
# Description:
#   Checks for "U+" and converts the given string to a hex-number.
#
# Register Usage:
#   Function Input Variables (.0: syscall, .1: getHexNum)
#     $a0: char *inBuffer
#   Function Output Variables (.0: syscall, .1: getHexNum)
#     $v0: int codePointTotal
#   Temporary Variables
#     $t0: 
#	.0: &inBuffer
#	.1: &getHexNumErrorMessage
#     $t1: char evalChar
#     $t9: int codePointTotal
# 
# Revision History:
#   2012.02.18	Andrew Blackton <aabxvc@mail.umkc.edu>	getHexNum's Birthday
#--------------------------------------------------
.globl getHexNum

.text
getHexNum:
	move	$t0, $a0		# (t0.0 <- a0.1) Move input variable char *inBuffer

	lbu	$t1, ($t0)		# (t0.0) char evalChar = *inBuffer
	bne	$t1, 'U', getExpectedU	# if evalChar != 'U' branch to getExpectedU
	addiu	$t0, $t0, 1		# (t0.0) inBuffer +=1

	lbu	$t1, ($t0)		# (t0.0) char evalChar = *inBuffer
	bne	$t1, '+', getExpectedP	# if evalChar != '+' branch to getExpectedP

	addiu	$t0, $t0, 1		# (t0.0) inBuffer +=1
	lbu	$t1, ($t0)		# (t0.0) char evalChar = *inBuffer
	move	$t9, $0			# int codePointTotal = 0

getHexNumCalc:
	beqz	$t1, getHexNumReturn	# if evalChar == 0 then return and exit
	sll	$t9, $t9, 4		# codePointTotal <<= 4 multiply by 16

	blt	$t1, 0x30, getHexNumIllegal	# if evalChar < '0x30' ('0') then exit('Illegal digit')
	ble	$t1, 0x39,getHexNumLow		# if 0x30 ('0') <= evalChar <= 0x39 ('9') then branch to getHexNumLow
	blt	$t1, 0x41, getHexNumIllegal	# if evalChar < 0x41 ('A') then exit('Illegal digit')
	ble	$t1, 0x46, getHexNumHigh	# if 'A'(0x41) <= evalChar <= 'F'(0x46) then branch to getHexNumHigh
	b	getHexNumIllegal		# else exit('Illegal digit')

getHexNumLow:
	addu	$t9, $t9, $t1		# codePointTotal += evalChar
	subiu	$t9, $t9, 0x30		# codePointTotal -= '0'
	b	getHexNumCalcCont

getHexNumHigh:
	subiu	$t1, $t1, 0x41		# evalChar -= 'A'
	addi	$t1, $t1, 10		# evalChar += 10
	addu	$t9, $t9, $t1		# codePointTotal += evalChar

getHexNumCalcCont:
	addiu	$t0, $t0, 1		# (t0.0) inBuffer +=1
	lbu	$t1, ($t0)		# (t0.0) char evalChar = *inBuffer
	b getHexNumCalc
	
getHexNumReturn:
	move	$v0, $t9		# return(codePointTotal)
	jr	$ra			# return from getHexNum()

	.data

getHexNumFuncName:	.asciiz	"\n:: ---- :: Error :: ---- :: ---- :: getHexNum :: ---- ::\n"
getHexNumExpectU:	.asciiz	"Incorrect Unicode Code Point Input Format, missing U"
getHexNumExpectP:	.asciiz	"Incorrect Unicode Code Point Input Format, missing +"
getHexNumIllegalOp:	.asciiz	"Incorrect Unicode Code Point Input Format, illegal digit"

	.text
# getHexNum Error Handling - will exit w/ error after entering exception handler
# Load Error Message String
# Register Usage:
#	$t0: Address of Error Message to print
getExpectedU:
	la	$t0, getHexNumExpectU		# (t0.1) Load Error Message Address
	b	getHexNumPrintError

getExpectedP:
	la	$t0, getHexNumExpectP		# (t0.1) Load Error Message Address
	b	getHexNumPrintError

getHexNumIllegal:
	la	$t0, getHexNumIllegalOp		# (t0.1) Load Error Message Address
	b	getHexNumPrintError

getHexNumPrintError:
	li	$v0, 4				# (v0.0) print string
	la	$a0, getHexNumFuncName		# (a0.0 <- t0) address of string to print
	syscall					# print null-terminated string

	move	$a0, $t0			# (a0.0) move address of error number to print
	li	$v0, 4				# (v0.0) print string
	syscall

	li	$v0, 4				# (v0.0) print string
	la	$a0, getHexNumFuncName		# (a0.0 <- t0) address of string to print
	syscall					# print null-terminated string

	li	$v0, 17				# (v0.0) syscall 17 - exit2 terminate w/ value
	li	$a0, 1				# (a0.0) exit w/ error code 1 (error)
	syscall
