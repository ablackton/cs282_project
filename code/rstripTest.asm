.data
testing0:	.asciiz	" \n \t \r "
line:	.asciiz	"This is a random line\nWith just one end\r\t"
.text

rstripTest:
	# Print string "line"
	li	$v0, 4
	la	$a0, line
	syscall
	# Print string "line"
	li	$v0, 4
	la	$a0, line
	syscall

	la	$a0, line	# initialize *buffer = &line for rstrip function call
	jal	rstrip		# call rstrip(&line)

	# Print string "line"
	# After being modified by rstrip
	li	$v0, 4
	la	$a0, line
	syscall
	# Print string "line" again
	# After being modified by rstrip
	li	$v0, 4
	la	$a0, line
	syscall

	# Print string "testing0"
	li	$v0, 4
	la	$a0, testing0
	syscall
	# Print string "testing0"
	li	$v0, 4
	la	$a0, testing0
	syscall

	la	$a0, testing0	# initialize *buffer = &testing0 for rstrip function call
	jal	rstrip		# call rstrip(&testing0)

	# Print string "testing0"
	# After being modified by rstrip
	li	$v0, 4
	la	$a0, testing0
	syscall
	# Print string "testing0" again
	# After being modified by rstrip
	li	$v0, 4
	la	$a0, testing0
	syscall

	li	$v0, 10
	syscall

