# CS282 Team Project Phase 1 - Encoding
#--------------------------------------------------
# Driver Function
# Name: main
# ********************
#
# Input Parameters:
#   * a processing code (e|d)
#   * input file name
#   * output file name
#
# Description:
# * Input Parameters from command line
# * Allocate an input buffer 65,536 (0x10000) bytes
# * Allocate an output buffer 65,536 (0x10000) bytes
# * Call Function: getFileNames
# * Read the input file into the input buffer 
#   - variable needed from syscall 14: number of bytes read
#     * Allows you determine if there was an I/O error
#     * Allows you to find the end of the input buffer
# * Close input file (file descriptor)
# * call function: encoder
# * Compute how many bytets to write to the output file
# * Write the output buffer to the output file 
# * Check for I/O errors and close output file
# 
# Register Usage:
#   Function Input Variables (.0: syscall, .1: main, .2 getFileNames, .3 encoder)
#     $a0: int argc
#     $a1: char *argv[]
#   Function Output Variables (.0: syscall, .2: getFileNames, .3 encoder)
#     $v0:
#       .2: int fdInputFile
#     $v1: return fdOutputFile
#   Saved Variables
#     $s0: int argc
#     $s1: char *argv[]
#     $s2: int fdInputFile
#     $s3: int fdOutputFile
#     $s6: &endOutputBuffer
#     $s7: &outputBuffer
#     $s8: &endInputBuffer
#     $s9: &inputBuffer
#
# Revision History:
#   2012.02.22	Andrew Blackton <aabxvc@mail.umkc.edu>	Main Driver Function Written
#--------------------------------------------------
	.data
space0	.word	0:1
inputBuffer:	.space 65536		# Allocate an input  buffer 65,536 (0x10000) bytes
space1	.word	0:1
outputBuffer:	.space 65536		# Allocate an output buffer 65,536 (0x10000) bytes

	.globl	main

	.text
main:
	move	$s0, $a0		# (s0 <- a0.1) # save int argc
					# number of program input arguments
	move	$s1, $a1		# (s1 <- a1.1) # save *argv[]
					# address of programs input arguments exit:

	# Call Fuction: getFileNames
	# Arguments:
	#	$a0: int argc
	#	$a1: &argv
	move	$a0, $s0		# (a0.2) initialize getFileName's first input variable
	move	$a1, $s1		# (a1.2) initialize getFileName's second input variable
	jal	getFileNames		# call getFileNames(argc, &argv)
	move	$s2, $v0		# (s2 <- v0.2) save input file's file descriptor
	move	$s3, $v1		# (s3 <- v1.2) save output file's file descriptor

	la	$s9, inputBuffer	# initialize pointer reference for inputBuffer

	# Read the input file into the input buffer
	# syscall 14 (read from file)
	# Arguments:
	#	$a0: file descriptor of file to be read
	#	$a1: address of input buffer
	#	$a2: maximum number of characters to read (0x10000)
	# Returns:
	#	$v0: contains number of characters read (0 if end-of-file, negative if error)
	li	$v0, 14			# (v0.0) syscall 14 - read from file
	move	$a0, $s2		# (a0.0 <- s2) initialize file descriptor of file to be read
	move	$a1, $s9		# (a1.0) initialize destination for input file's contents
	li	$a2, 0x10000		# (a2.0) maxiumum number of characters to read from input file
	syscall				# read from file
	move	$t0, $v0		# (t0 <- v0.0) read from file return value
	bltz	$t0, errorReadingFile	# if return value < 0 error encountered then exit
	beqz	$t0, errorNoInputFile	# if return value == 0 error encountered then exit
	addu	$s9, $s9, $t0		# find address - end of inputBuffer
	
	# Close Input File
	li	$v0, 16			# (v0.0) syscall 16 - close file
	move	$a0, $s2		# (a0.0 <- s2) initialize file descriptor of file to be closed
	syscall				# close inputFile handle

	# Call function: encoder
	# Arguments:
	#	$a0: &inBuffer
	#	$a1: &endInputBuffer
	#	$a2: &outBuffer
	# Returns:
	#	$v0: &outBuffer - end
	la	$s7, outputBuffer
	move	$a0, $s9		# (a0.3 <- s9) initialize encoder's arguments &inputBuffer
	move	$a1, $s8		# (a1.3 <- s9) initialize encoder's arguments &endInputBuffer
	move	$a2, $s7		# (a1.3 <- s9) initialize encoder's arguments &ouputBuffer
	jal	encoder			# call encoder(&inputBuffer, &endInputBuffer, &outputBuffer)
	move	$s6, $v0		# *endOutputBuffer = return(outputBuffer)

	# Write the output buffer's contents to the output file
	# syscall 15 - write to file
	# Arguments:
	#	$a0: file descriptor of file to be written
	#	$a1: address of output buffer
	#	$a2: number of characters to write
	# Returns:
	#	$v0: contains number of characters written (negative if error)
	move	$a0, $s3		# (a0.0 <- s3) file descriptor of file to be written fdOutputFile
	move	$a1, $s7		# (a1.0 <- s3) address of output buffer
	subu	$a2, $s6, $s7		# number of characters to write = &endOutputBuffer - &outputBuffer
	syscall				# write to file
	bltz	$v0, errorWritingToFile	# if return is negative then error writing to file

	# Close Output File
	li	$v0, 16			# (v0.0) syscall 16 - close file
	move	$a0, $s3		# (a0.0 <- s3) initialize file descriptor of file to be closed
	syscall				# close inputFile handle

exit:
	li	$v0, 10			# (v0.0) terminate execution
	syscall				# graceful termination

	.data
mainErrorFuncName:	.asciiz	"\n:: ---- :: Error :: ---- :: ---- :: main :: ---- ::\n"
mainErrorReadingFile:	.asciiz "::  Error encountered while reading input file   ::
mainErrorWritingFile:	.asciiz "::  Error encountered while writing output file  ::
mainErrorNullInput:	.asciiz "::  Input File is Empty                          ::
	.text
# main Error Handling - will exit w/ error after entering exception handler
# Load Error Message String
# Register Usage:
#	$t0: Address of Error Message to print
errorReadingFile:
	la	$t0, mainErrorReadingFile	# (t0.1) Load Error Message Address
	b	mainPrintError

errorNoInputFile:
	la	$t0, mainErrorNullInput		# (t0.1) Load Error Message Address
	b	mainPrintError

errorWritingToFile:
	la	$t0, errorWritingFile		# (t0.1) Load Error Message Address
	b	mainPrintError

mainPrintError:
	li	$v0, 4				# (v0.0) print string
	la	$a0, mainErrorFuncName		# (a0.0 <- t0) address of string to print
	syscall					# print null-terminated string

	move	$a0, $t0			# (a0.0) move address of error number to print
	li	$v0, 4				# (v0.0) print string
	syscall

	li	$v0, 4				# (v0.0) print string
	la	$a0, mainErrorFuncName		# (a0.0 <- t0) address of string to print
	syscall					# print null-terminated string
	b	error

error:
	li	$v0, 17			# (v0.0) syscall 17 - exit2 terminate w/ value
	li	$a0, 1			# (a0.0) exit w/ error code 1 (error)
	syscall

#--------------------------------------------------
# Function
# Name: encoder
# ********************
# char* encoder(char* inBuffer, char* end, char* outBuffer)
#
# Description:
#   TODO
# char* encoder(char *inBuffer char* end, char* outBuffer)
#   while inBuffer < end
#   inBuffer = getLine(inBuffer, lineBuffer)
#   rstrip(lineBuffer)
#   value = getHexNum(lineBuffer)
#   outBuffer = encode(value, outBuffer)
# return outBuffer
# 
# Revision History:
#   2012.02.06-00: Birthday	Origional Author's Name <umkcEmail@mail.umkc.edu>
#--------------------------------------------------
encoder:
# TODO
	jr	$ra

#--------------------------------------------------
# Function
# Name: rstrip
# ********************
# void rstrip(char *buffer) {
#   cursor = buffer
#   while *cursor != ‘\0’:
#     cursor += 1
#     cursor -= 1
#   while *cursor = ‘\n’ or ‘\r’ or ‘\t’ or ‘ ‘:
#     *cursor = ‘\0’
#     cursor -= 1
# }
#
# Description:
#   Strip the trailing '\r\n' or '\n' from the line buffer.
# 
# Revision History:
#   2012.02.06-00: Birthday	Origional Author's Name <umkcEmail@mail.umkc.edu>
#--------------------------------------------------
rstrip:
# TODO
# This function takes a null-terminated string as parameter. Your function should 
# read the bytes in the string until it finds the NUL. Then it should back up one 
# space at a time, overwriting each whitespace character (blank, tab, carriage 
# return, or newline) it finds by NUL. Once it finds a character that is not 
# whitespace, it should stop, so that only white space characters are stripped

	jr	$ra

#--------------------------------------------------
# Function
# Name: getOpenFile
# ********************
# int getOpenFile(char *filename, int mode)
# 
# Description:
#   Wrapper around syscall 13.  Opens a file in the indicated mode (read/write)
#   and returns the file descriptor of the newly-opened file.  In the case of 
#   an I/O error, prints an error message and aborts.
# 
# NOTE: It's crucial that you write your error message to the console, using syscall 4.
# 
# Revision History:
#   2012.02.06-00: Birthday	Origional Author's Name <umkcEmail@mail.umkc.edu>
#--------------------------------------------------
getOpenFile:
# TODO
	jr	$ra

#--------------------------------------------------
# Function
# Name: getFileNames
# ********************
# (int fdIn, int fdOut) getFileNames(int argc, char *argv[])
#
# Description:
#   Get the file names and return the file descriptors of the open files in the v-registers.
# 
# Revision History:
#   2012.02.06-00: Birthday	Origional Author's Name <umkcEmail@mail.umkc.edu>
#--------------------------------------------------
getFileNames:
# TODO
# Provide program arguments to the MIPS program. Default value is false. New in Release 3.5. If selected, a text field will appear at the top of the Text Segment Display. Any argument values in this text field at the time of program execution will be stored in MIPS memory prior to execution. The argument count (argc) will be placed in register $a0, and the address of an array of null-terminated strings containing the arguments (argv) will be placed in register $a1. These values are also available on the runtime stack ($sp). 
	jr	$ra

#--------------------------------------------------
# Function
# Name: getLine
# ********************
# char* getLine(char *inBuffer, char *lineBuffer)
#   cursor1 = inBuffer
#   cursor2 = lineBuffer
#   repeat:
#     c = *cursor1
#     *cursor2 = c
#     cursor1 += 1
#     cursor2 += 1
#   until c == ‘\n’
# return cursor1
#
# Description:
#   Copies one line of input from the input buffer to a lineBuffer and
#   returns an updated pointer to the input buffer.
#
# Register Usage:
#   Function Input Variables (.0: syscall, .1: getLine)
#     $a0: char *inBuffer     (input variable - pointer to input buffer)
#     $a1: char *lineBuffer   (input variable - pointer to line buffer)
#   Function Output Variables (.0: syscall, .1: getLine)
#     $v0: return *inBuffer (return - updated address to inBuffer)
#   Temporary Variables
#     $t1: char *inputBuffer
#     $t2: char *lineBuffer
#     $t9: char inputBufferChar
# 
# Revision History:
#   2012.02.06-00: Birthday	Origional Author's Name <umkcEmail@mail.umkc.edu>
#--------------------------------------------------
getLine:
	move	$t0, $a0	# (t0 <- a0.1) Initialize Address of inputBuffer
	move	$t1, $a1	# (t1 <- a1.1) Initialize Address of lineBuffer

getLineLoop:
	lbu	$t9, ($t0)	# inputBufferChar = *inputBuffer
	sb	$t9, ($t1)	# *lineBuffer = inputBufferChar
	addi	$t0, $t0, 1	# *inputBuffer++  Increment inputBuffer address by a byte
	addi	$t1, $t1, 1	# *lineBuffer++  Increment lineBuffer address by a byte
	beq	$t9, '\n', getLineExit	# exit function when inputBufferChar = '\n'
	j	getLineLoop

getLineExit:
	move	$v0, $t0	# return(*inputBuffer)
	jr	$ra		# exit getLine()

#--------------------------------------------------
# Function
# Name: getHexNum
# ********************
# int getHexNum(char* inBuffer)
#   if *inBuffer != ‘U’:
#     exit('Expected U’)
#   inBuffer += 1
#   if *inBuffer != ‘+’:
#     exit(“expected +”)
#   codePointTotal = 0
#   inBuffer += 1
#   while *inBuffer != 0:
#     codePointTotal <<= 4 // multiply by 16
#     if ‘0’ < *inBuffer <= ‘9’:
#       codePointTotal += *inBuffer – ‘0’
#     else if ‘A’ <= *inBuffer <= ‘F’:
#       codePointTotal += *inBuffer - ‘A’ + 10
#     else: exit(‘Illegal digit”)
# return codePointTotal
#
# Description:
#   Checks for "U+" and converts the given string to a hex-number.
#
# Register Usage:
#   Function Input Variables (.0: syscall, .1: getHexNum)
#     $a0: char *inBuffer
#   Function Output Variables (.0: syscall, .1: getHexNum)
#     $v0: int codePointTotal
#   Temporary Variables
#     $t0: char *inBuffer
#     $t1: char evalChar
#     $t9: int codePointTotal
# 
# Revision History:
#   2012.02.06-00: Birthday	Origional Author's Name <umkcEmail@mail.umkc.edu>
#--------------------------------------------------
getHexNum:
	move	$t0, $a0		# (t0 <- a0.1) Move input variable char *inBuffer

	lbu	$t1, ($t0)		# char evalChar = *inBuffer
	bne	$t1, 'U', getExpectedU	# if evalChar != 'U' branch to getExpectedU
	addi	$t0, $t0, 1		# inBuffer +=1
	lbu	$t1, ($t0)		# char evalChar = *inBuffer
	bne	$t1, '+', getExpectedP	# if evalChar != '+' branch to getExpectedP
	addi	$t0, $t0, 1		# inBuffer +=1
	move	$t9, $zero		# int codePointTotal = 0

getHexNumCalc:
	beqz	$t1, getHexNumReturn	# if evalChar == 0 then return and exit
	sll	$t9, $t9, 4		# codePointTotal <<= 4 multiply by 16
	ble	$t1, '9', getHexNumLow	# if 0 < evalChar <= 9 then branch to getHexNumLow
	ble	$t1, 'F', getHexNumHigh	# if 0 < evalChar <= 9 then branch to getHexNumHigh
	b	getHexNumIllegal	# else exit('Illegal digit')

getHexNumLow:
	add	$t9, $t9, $t1		# codePointTotal += evalChar
	b	getHexNumCalcCont

getHexNumHigh:
	subi	$t1, $t1, 0x41		# evalChar -= 'A'
	addi	$t1, $t1, 10		# evalChar -= 10
	add	$t9, $t9, $t1		# codePointTotal += evalChar
	b	getHexNumCalcCont

getHexNumCalcCont:
	addi	$t0, $t0, 1		# inBuffer +=1
	lbu	$t1, ($t0)		# char evalChar = *inBuffer
	
	move	$v0, $t9		# return(codePointTotal)
	jr	$ra			# return from getHexNum()

	.data

getHexNumFuncName:	.asciiz	":: ---- :: Error :: ---- :: getHexNum :: ---- ::\n"
getHexNumExpectU:	.asciiz	"Incorrect Unicode Code Point Input Format, missing U\n"
getHexNumExpectP:	.asciiz	"Incorrect Unicode Code Point Input Format, missing +\n"
getHexNumIllegalOp:	.asciiz	"Incorrect Unicode Code Point Input Format, illegal digit\n"

	.text
# Load Error Message String
# Register Usage:
#	$t0: Address of Error Message to print
getExpectedU:
	la	$t0, getHexNumExpectU
	b	getHexNumPrintError

getExpectedP:
	la	$t0, getHexNumExpectP
	b	getHexNumPrintError

getHexNumIllegal:
	la	$t0, getHexNumIllegalOp
	b	getHexNumPrintError

getHexNumPrintError:
	jal	getHexNumPrintFuncName		# DEBUG: Print Function Name and Return

	move	$a0, $t0			# (a0.0) move address of error number to print
	li	$v0, 4				# (v0.0) print string
	syscall

	jal	getHexNumPrintFuncName		# DEBUG: Print Function Name and Return

	li	$v0, 17				# (v0.0) syscall 17 - exit2 terminate w/ value
	li	$a0, 1				# (a0.0) exit w/ error code 1 (error)
	syscall

getHexNumPrintFuncName:
	li	$v0, 4				# (v0.0) print string
	la	$a0, getHexNumFuncName		# (a0.0 <- t0) address of string to print
	syscall					# print null-terminated string
	jr	$ra

#--------------------------------------------------
# Function
# Name: encode
# ********************
# char* encode(int codePoint, char *outBuffer)
#
# Description:
#   TODO
#   
# Register Usage:
#   Function Input Variables (.0: syscall, .1: encode)
#     $a0: int codePoint
#     $a1: char *outBuffer   (input variable - pointer to output buffer)
#   Function Output Variables (.0: syscall, .1: encode)
#     $v0: return *outBuffer (return - updated pointer to output buffer)
#   Temporary Variables
#     $t0: int codePoint
#     $t1: char *outBuffer
#     $t2: int codePointB0
#     $t3: int codePointB1
#     $t4: int codePointB2
#     $t8: int encodedValueSize
#     $t9: int encodedValue
# 
# Revision History:
#   2012.02.06-00: Birthday	Origional Author's Name <umkcEmail@mail.umkc.edu>
#--------------------------------------------------
encode:
	move	$t0, $a0		# (t0 <- a0.1) Move input variable int codePoint
	move	$t1, $a1		# (t1 <- a1.1) Move input variable char *outBuffer
	
	blt	$t0, 0x80, encode1	# if(codePoint<0x80) encode single byte
	blt	$t0, 0x800, encode2	# if(codePoint<0x800) encode two bytes
	blt	$t0, 0x10000, encode3	# if(codePoint<0x10000) encode three bytes
	blt	$t0, 0x110000, encode4	# if(codePoint<0x110000) encode four bytes
	b	encodeAbort		# invalid input codePoint

encode1:
	andi	$t9, $t0, 0x7F		# encodedValue = codePoint && 0x7F
					# Single Byte Mask 0x7F = 0111 1111
	li	$t8, 1			# encodedValueSize = 1
	j	writeBuffer

encode2:
	bgt	$t0, 0xDFFF, encode2Cont # if (codePoint >= 0x800 && codePoint <= 0xDFFF) 
	bge	$t0, 0x0800, encodeAbort # invalid codePoint Value, abort function

encode2Cont:
	sll	$t2, $t0, 2		# codePointB0 = codePoint<<2
	andi	$t2, $t2, 0x1F00	# codePointB0 = codePointB0 && 0x1F00
					# First Byte Mask:
					#   0x1F00 = 0001 1111 0000 0000
	andi	$t3, $t0, 0x3F		# codePointB1 = codePoint && 0x3F
					# Second Byte Mask:
					#   0x3F = 0000 0000 0011 1111
	ori	$t9, $t2, 0xC080	# encodedValue = codePointB0 || 0xC080
					# Protocol Two-Byte Mask: 
					#   0xC080 = 1100 0000 1000 0000
	or	$t9, $t9, $t3		# encodedValue = codePointB0 || codePointB1 || 0x0C080
	li	$t8, 2			# encodedValueSize = 2
	j	writeBuffer

encode3:
	sll	$t2, $t0, 4		# codePointB0 = codePoint<<4
	andi	$t2, $t2, 0xF0000	# codePointB0 = codePointB0 && 0x0F0000
					# First Byte Mask:
					#   0x0F0000 = 0000 1111 0000 0000 0000 0000
	sll	$t3, $t0, 2		# codePointB1 = codePoint<<2
	andi	$t3, $t3, 0x3F00	# codePointB1 = codePointB1 && 0x3F00
					# Second Byte Mask:
					#   0x3F00 = 0000 0000 0011 1111 0000 0000
	andi	$t4, $t0, 0x3F		# codePointB2 = codePoint && 0x3F
					# Third Byte Mask:
					#   0x3F = 0000 0000 0011 1111
	ori	$t9, $t2, 0xE08080	# encodedValue = codePointB0 || 0xE08080
					# Protocol Three-Byte Mask: 
					#   0xE08080 = 1110 0000 1000 0000 1000 0000
	or	$t9, $t9, $t3		# encodedValue = encodedValue || codePointB1
	or	$t9, $t9, $t4		# encodedValue = encodedValue || codePointB2
	li	$t8, 3			# encodedValueSize = 3
	j	writeBuffer

encode4:
	sll	$t2, $t0, 6		# codePointB0 = codePoint<<6
	andi	$t2, $t2, 0x7000000	# codePointB0 = codePointB0 && 0x7000000
					# First Byte Mask:
					#   0x7000000 = 0000 0111 0000 0000 0000 0000 0000 0000
	sll	$t3, $t0, 4		# codePointB1 = codePoint<<4
	andi	$t3, $t3, 0x3F0000	# codePointB1 = codePointB1 && 0x3F0000
					# Second Byte Mask:
					#   0x03F0000 = 0000 0000 0011 1111 0000 0000 0000 0000
	sll	$t4, $t0, 2		# codePointB2 = codePoint<<4
	andi	$t4, $t4, 0x3F00	# codePointB2 = codePointB2 && 0x3F00
					# Third Byte Mask:
					#   0x0003F00 = 0000 0000 0000 0000 0011 1111 0000 0000
	andi	$t9, $t0, 0x3F		# encodedValue = codePoint && 0x3F
					# Fourth Byte Mask
					#   0x000003F = 0000 0000 0000 0000 0000 0000 0011 1111
	ori	$t9, $t9, 0xF0808080	# encodedValue = encodedValue || 0xF0808080
					# Protocol Four-Byte Mask
					#  0xF0808080 = 1111 0000 1000 0000 1000 0000 1000 0000
	or	$t2, $t2, $t3		# codePointB0 = codePointB0 || codePointB1
	or	$t9, $t9, $t4		# encodedValue = encodedValue || codePointB2
	or	$t9, $t9, $t2		# encodedValue = encodedValue || codePointB0
	li	$t8, 4			# encodedValueSize = 4
	j	writeBuffer

writeBuffer:

updateBuffer:

encodeAbort:

# if value is illegal, abort with error message
#   - larger than 0x10FFFF
#   - Invalid Range: U+D800-U+DFFF
# determine how many bytes to write from the size of the value:
#   0      - 0x7F:	1 byte
#   0x80   - 0x7FF:	2 byte
#   0x800  - 0x7FF:	3 byte
#   0x1000 - 0x10FFFF:	4 byte
# When you have encoded the last value, the difference between the pointer returned
# by encode and the start of the output buffer is the number of bytes you have to 
# write to the output file.

# pick out the appropriate bits and write the code bytes, most significant first
# algorithm:
# if (codePoint<0x80) 
# 
# Mask Values
# 0x7F = 0111 1111
#
# Input:  codePoint   = 0aaa aaaa
# Encode: codePointB0 = 0aaa aaaa
#
# encodedValue = 0x7F
# encodedValue = encodedValue && codePoint
#
# else if (codePoint<0x0800)
#   if (codePoint > 0xD800 && codePoint < 0xDFFF)
#     Illegel codePoint value - Abort with error
#   else
#
# Mask Values
# 0x1F3F	= 0001 1111 0011 1111
# 0xC080	= 1100 0000 1000 0000
# codePoint	= 0000 0aaa aabb bbbb
# encodedValue	= 110a aaaa 10bb bbbb
# codePointB0	= 110a aaaa 0000 0000
# codePointB1	= 0000 0000 10bb bbbb
#
# codePointB0 = codePoint<<2
# codePointB0 = codePointB0 && 0x1F00
# codePointB1 = codePoint && 0x003F
# encodedValue = codePointB0 || codePointB1 || 0xC080
#
# else if (codePoint<0x010000)
#
# Mask Values
# 0x0F3F3F	= 0000 1111 0011 1111 0011 1111
# 0xE08080	= 1110 0000 1000 0000 1000 0000
#
# codePoint	= 0000 0000 aaaa bbbb bbcc cccc
# encodedValue	= 1110 aaaa 10bb bbbb 10cc cccc
# codePointB0	= 1110 aaaa 0000 0000 0000 0000
# codePointB1	= 0000 0000 10bb bbbb 0000 0000
# codePointB2	= 0000 0000 0000 0000 10cc cccc
#
# codePointB0 = codePoint<<4
# codePointB0 = codePointB0 && 0x0F0000
# codePointB1 = codePoint<<2
# codePointB1 = codePointB1 && 0x003F00
# codePointB2 = codePoint<<0
# codePointB2 = codePointB2 && 0x00003F
# encodedValue = codePointB0 || codePointB1 || 0xE08080
#
# else if (codePoint<0x110000)
#
# Mask Values
# 0x073F3F3F	= 0000 0111 0011 1111 0011 1111 0011 1111
# 0xF0808080	= 1111 0000 1000 0000 1000 0000 1000 0000
#
# codePoint	= 0000 0000 000a aabb bbbb cccc ccdd dddd
# encodedValue	= 1111 0aaa 10bb bbbb 10cc cccc 10dd dddd
# codePointB0	= 1111 0aaa 0000 0000 0000 0000 0000 0000
# codePointB1	= 0000 0000 10bb bbbb 0000 0000 0000 0000
# codePointB2	= 0000 0000 0000 0000 10cc cccc 0000 0000
# codePointB3	= 0000 0000 0000 0000 0000 0000 10dd dddd
#
# codePointB0 = codePoint<<6
# codePointB0 = codePointB0 && 0x07000000
# codePointB1 = codePoint<<4
# codePointB1 = codePointB1 && 0x003F0000
# codePointB2 = codePoint<<2
# codePointB2 = codePointB2 && 0x00003F00
# codePointB3 = codePoint<<0
# codePointB3 = codePointB3 && 0x0000003F
# encodedValue = codePointB0 || codePointB1 || codePointB2 || 0xF0808080
#
#    end if
	jr	$ra
