# pick out the appropriate bits and write the code bytes, most significant first
# algorithm:
# ---- ---- ---- ----
# Mask Values
#	0x7F = 0111 1111
# ---- ---- ---- ----
# Input:  
#	codePoint   = 0aaa aaaa
#	codePointB0 = 0aaa aaaa
# ---- ---- ---- ----
if (codePoint<0x80) 
	encodedValue = 0x7F
	encodedValue = encodedValue && codePoint
	outBuffer	 = encodedValue
	
# ---- ---- ---- ----
# Mask Values
#	0x1F3F	= 0001 1111 0011 1111
#	0xC080	= 1100 0000 1000 0000
# ---- ---- ---- ----
#	codePoint	= 0000 0aaa aabb bbbb
#	codePointB0	= 0000 0000 110a aaaa
#	codePointB1	= 0000 0000 10bb bbbb
#	encodedValue= 110a aaaa 10bb bbbb
# ---- ---- ---- ----
else if (codePoint<0x0800)
  if (codePoint > 0xD800 && codePoint < 0xDFFF)
    Illegel codePoint value - Abort with error
  else
	codePointB0 = codePoint>>6
	codePointB0 = codePointB0 && 0x1F
	codePointB0 = codePointB0 || 0xC0

	codePointB1 = codePoint && 0x3F
	codePointB1 = codePoint || 0x80
	outBuffer = codePointB0
	*outBuffer++
	outBuffer = codePointB1
	*outBuffer++
# ---- ---- ---- ----
#	Mask Values
#	0x0F3F3F	= 0000 1111 0011 1111 0011 1111
#	0xE08080	= 1110 0000 1000 0000 1000 0000
# ---- ---- ---- ----
#	codePoint		= 0000 0000 aaaa bbbb bbcc cccc
#	codePointB0		= 0000 0000 0000 0000 1110 aaaa
#	codePointB1		= 0000 0000 0000 0000 10bb bbbb
#	codePointB2		= 0000 0000 0000 0000 10cc cccc
#	encodedValue		= 1110 aaaa 10bb bbbb 10cc cccc
# ---- ---- ---- ----
else if (codePoint<0x010000)
	codePointB0 = codePoint>>12
	codePointB0 = codePointB0 && 0x0F
	codePointB0 = codePointB0 || 0xE0
	codePointB1 = codePoint>>6
	codePointB1 = codePointB1 && 0x3F
	codePointB1 = codePointB1 || 0x80
	codePointB2 = codePoint
	codePointB2 = codePointB2 && 0x3F
	codePointB2 = codePointB2 || 0x80
	outBuffer = codePointB0
	*outBuffer++
	outBuffer = codePointB1
	*outBuffer++
	outBuffer = codePointB2
	*outBuffer++

# ---- ---- ---- ----
#	Mask Values
#	0x073F3F3F	= 0000 0111 0011 1111 0011 1111 0011 1111
#	0xF0808080	= 1111 0000 1000 0000 1000 0000 1000 0000
# ---- ---- ---- ----
#	codePoint	= 0000 0000 000a aabb bbbb cccc ccdd dddd
#	codePointB0	= 0000 0000 0000 0000 0000 0000 1111 0aaa
#	codePointB1	= 0000 0000 0000 0000 0000 0000 10bb bbbb
#	codePointB2	= 0000 0000 0000 0000 0000 0000 10cc cccc
#	codePointB3	= 0000 0000 0000 0000 0000 0000 10dd dddd
#	encodedValue= 1111 0aaa 10bb bbbb 10cc cccc 10dd dddd
# ---- ---- ---- ----
else if (codePoint<0x110000)
    
     codePointB0 = codePoint>>18
     codePointB0 = codePointB0 && 0x07
     codePointB0 = codePointB0 || 0xF0
     codePointB1 = codePoint>>12
     codePointB1 = codePointB1 && 0x3F
     codePointB1 = codePointB1 || 0x80
     codePointB2 = codePoint>>6
     codePointB2 = codePointB2 && 0x3F
     codePointB2 = codePointB2 || 0x80
     codePointB3 = codePoint>>0
     codePointB3 = codePointB3 && 0x3F
     codePointB3 = codePointB3 || 0x80
     outBuffer = codePointB0
     *outBuffer++
     outBuffer = codePointB1
     *outBuffer++
     outBuffer = codePointB2
     *outBuffer++
     outBuffer = codePointB3
     *outBuffer++
end if

return *outBuffer
