#--------------------------------------------------
# getOpenFile.asm
# Function
# Name: getOpenFile
# ********************
# int getOpenFile(char *filename, int mode)
# 
# Description:
#   Wrapper around syscall 13.  Opens a file in the indicated mode (read/write)
#   and returns the file descriptor of the newly-opened file.  In the case of 
#   an I/O error, prints an error message and aborts.
# 
# NOTE: It's crucial that you write your error message to the console, using syscall 4.
#
# Register's Used:
#	$t0:
# 
# Revision History:
#   2012.02.23	Andrew Blackton <aabxvc@mail.umkc.edu>	Finished getOpenFile Function
#--------------------------------------------------
.globl	getOpenFile
.data
getOpenFileFunctionName:	.asciiz	":: ---- :: getOpenFile :: ---- ::"
getOpenFileErrorFunctionName:	.asciiz "\n:: ---- :: Error :: ---- :: ---- :: getOpenFile :: ---- ::\n"
getOpenFileErrorFailed:	.asciiz "Unable to open file: "
.text
getOpenFile:
	move	$t0, $a0

	#li	$v0, 4				# (v0.0) print string
	#move	$a0, $t0			# (a0.0) 
	#syscall

	# syscall 13 - open file
	# Arguments
	# $a0: address of null-terminated string containing filename
	# $a1: flags (0=read, 1=write)
	# $a2: mode (ignored)
	# Returns
	# $v0: contains file descriptor (negative if error)
	li	$v0, 13		# syscall 13 - open file
	move	$a0, $t0
	syscall
	bltz	$v0, getOpenFileFail
	jr	$ra		

getOpenFileFail:
	li	$v0, 4				# (v0.0) print string
	la	$a0, getOpenFileErrorFunctionName # (a0.0 <- t0) address of string to print
	syscall					# print null-terminated string

	la	$a0, getOpenFileErrorFailed	# (a0.0) move address of error number to print
	li	$v0, 4				# (v0.0) print string
	syscall

	move	$a0, $t0			# (a0.0) 
	li	$v0, 4				# (v0.0) print string
	syscall

	li	$v0, 4				# (v0.0) print string
	la	$a0, getOpenFileErrorFunctionName # (a0.0 <- t0) address of string to print
	syscall					# print null-terminated string
	b	error

error:
	li	$v0, 17			# (v0.0) syscall 17 - exit2 terminate w/ value
	li	$a0, 1			# (a0.0) exit w/ error code 1 (error)
	syscall

