#--------------------------------------------------
# getLineTest.asm
# Function
# Name: getLineTest
# ********************
#
# Description:
# * Assign hardcoded input buffer (.ascii w/o null terminator) - *inBuffer
# * Allocate memory space for lineBuffer (1 word should suffice to hold each line) - *lineBuffer
# * Call getLine function three times 
#	* getLine(*inBuffer, *lineBuffer)
#	* print *lineBuffer Contents
#	* clear *lineBuffer contents
# 
# Expected Output:
# This is line 1
# This is line 2
# This is line 3
#
# Register Usage:
#   Function Input Variables (.0: syscall, .1: getLine)
#     $a0: 
#       a0.0: syscall argument values
#       a0.1: getLine input character
#     $a1: getLine input character
#   Function Output Variables (.0: syscall, .1: getLine)
#     $v0:
#       v0.0: syscall service number
#       v0.1: getLine return character
#   Saved Variables
#     $s0: char *inBuffer
#     $s1: char *lineBuffer
#     $s2: int loopCount
#
# Revision History:
#   2012.02.13	Andrew Blackton <aabxvc@mail.umkc.edu>	Created
#--------------------------------------------------
	.data
inBuffer:	.ascii "This is line 1\r\nThis is line 2\nThis is line 3\n"
lineBuffer:	.word 0:4
	.text
# Register Usage:
# Function Input Variables (.0: syscall, .1: getLine)
#   $a0.1: getLine input: *inBuffer
# Function Output Variables (.0: syscall, .1: getLine)
#   $v0.1: getLine output: *inBuffer
# Saved Variables
#   $s0: *inBuffer
#   $s1: *lineBuffer
#   $s2: int loopCount
getLineTest:
	la	$s0, inBuffer		# char *inBuffer initialize inBuffer Address
	la	$s1, lineBuffer		# char *lineBuffer initialize lineBuffer Address
	
	li	$s2, 3			# int loopCount = 3

mainLoop:
	move	$a0, $s0		# (a0.1 <- s0) getLine input: *inBuffer
	move	$a1, $s1		# (a1.1 <- s1) getLine input: *lineBuffer
	jal	getLine			# getLine(*inBuffer, *lineBuffer)

	move	$s0, $v0		# (s0 <- v0.1) *inBuffer = getLine(*inBuffer, *lineBuffer)
	li	$v0, 4                  # (v0.0) syscall print string
	move	$a0, $s1                # (a0.0) print string at *lineBuffer
	syscall

	addi	$s2, $s2, -1		# loopCount--

	beqz	$s2, exit		# if loopCount == 0 then exit

clearLineBuffer:
	sw	$0, ($s1)		# clear first byte in line buffer
	sw	$0, 4($s1)		# clear second byte in line buffer
	sw	$0, 8($s1)		# clear third byte in line buffer
	sw	$0, 12($s1)		# clear fourth byte in line buffer

	j mainLoop

exit:
	li	$v0, 10			# (v0.0) terminate execution
	syscall				# graceful termination


