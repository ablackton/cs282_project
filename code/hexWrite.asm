#--------------------------------------------------
# hexWrite.asm
# Function
# Name: hexWrite
# ********************
# char* hexWrite(int hexNum, char* outputBuffer)
#
# Description:
# Takes an input hexNum and converts it into a UTF code
# point value.  Places the converted hexNum, the UTF code
# point, into the output buffer.
# 
# Input Registers:
# $a0: int hexNum
# $a1: &outputBuffer
# Register Returns:
# $v0: updated &outputBuffer
#
# Revision History:
#   2012.03.21	Andrew Blackton <aabxvc@mail.umkc.edu>	Creation
#--------------------------------------------------

.globl	hexWrite
.text
# $s0 Example Input Four Byte Segregation
#                   ( Byte Byte Byte Byte )
#                   ( three two one  zero )
#                   ( ----|----|----|---- )
#            0xABCD ( 1010 xxxx xxxx xxxx )
#        12>>0xABCD ( 0000 0000 0000 1010 )
#       0xF&&0xABCD ( 0000 0000 0000 1010 )
#                   ( ----|----|----|---- )
#            0xABCD ( xxxx 1011 xxxx xxxx )
#         8>>0xABCD ( 0000 0000 1010 1011 )
#       0xF&&0xABCD ( 0000 0000 0000 1011 )
#                   ( ----|----|----|---- )
#            0xABCD ( xxxx xxxx 1100 xxxx )
#         4>>0xABCD ( 0000 1010 1011 1100 )
#       0xF&&0xABCD ( 0000 0000 0000 1100 )
#                   ( ----|----|----|---- )
#            0xABCD ( xxxx xxxx xxxx 1101 )
#         0>>0xABCD ( 1010 1011 1100 1101 )
#       0xF&&0xABCD ( 0000 0000 0000 1101 )
#                   ( ----|----|----|---- )
#
.text
hexWrite:
	addiu	$sp, $sp, -32
	sw	$ra, ($sp)
	sw	$s0, 4($sp)
	sw	$s1, 8($sp)

	# Registers:
	# $s0: hexNum
	# $s1: &outputBuffer
	move	$s0, $a0	# store function input - hexNum
	move	$s1, $a1	# store function input - &outputBuffer

	li	$t0, 0x55	# load 'U'
	sb	$t0, ($s1)	# *outputBuffer = 'U'
	li	$t0, 0x2B	# load '+'
	sb	$t0, 1($s1)	# *(outputBuffer+1) = '+'
	addiu	$s1, $s1, 2	# outputBuffer += 2

	ble	$s0, 0xFFFF,   hexWrite.convert.four
	ble	$s0, 0xFFFFF,  hexWrite.convert.five
	#ble	$s0, 0x10FFFF, hexWrite.convert.six

hexWrite.convert.six:
	srl	$a0, $s0, 20		# hexNumByte5 = 20>>hexNum
	jal	convertByte		# convertByte(hexNumByte5)
	sb	$v0, ($s1)		# outputBuff[0] = char* convertByte(hexNumByte5)
	addiu	$s1, $s1, 1		# outputBuff++

hexWrite.convert.five:
	srl	$a0, $s0, 16		# hexNumByte4 = 16>>hexNum
	andi	$a0, $a0, 0xF		# hexNumByte4 = hexNumByte4&&0xF
	jal	convertByte		# convertByte(hexNumByte4)
	sb	$v0, ($s1)		# outputBuff[0] = char* convertByte(hexNumByte4)
	addiu	$s1, $s1, 1		# outputBuff++

hexWrite.convert.four:
	srl	$a0, $s0, 12		# hexNumByte3 = 12>>hexNum
	andi	$a0, $a0, 0xF		# hexNumByte3 = hexNumByte3 && 0xF
	jal	convertByte		# call convertByte(hexNumByte3)
	sb	$v0, ($s1)		# *outputBuff = char* convertByte(hexNumByte3)
	addiu	$s1, $s1, 1		# outputBuff++

	srl	$a0, $s0, 8		# hexNumByte2 = 8>>hexNum
	andi	$a0, $a0, 0xF		# hexNumByte2 = hexNumByte2 && 0xF
	jal	convertByte		# call convertByte(hexNumByte2)
	sb	$v0, ($s1)		# *outputBuff = char* convertByte(hexNumByte2)
	addiu	$s1, $s1, 1		# outputBuff++

	srl	$a0, $s0, 4		# hexNumByte1 = 4>>hexNum
	andi	$a0, $a0, 0xF		# hexNumByte1 = hexNumByte1 && 0xF
	jal	convertByte		# call convertByte(hexNumByte2)
	sb	$v0, ($s1)		# *outputBuff = char* convertByte(hexNumByte1)
	addiu	$s1, $s1, 1		# outputBuff++

	andi	$a0, $s0, 0xF		# hexNumByte0 = hexNum && 0xF
	andi	$a0, $a0, 0xF		# hexNumByte0 = hexNumByte0 && 0xF
	jal	convertByte		# call convertByte(hexNumByte0)
	sb	$v0, ($s1)		# *outputBuff = char* convertByte(hexNumByte0)
	addiu	$s1, $s1, 1		# outputBuff++

hexWrite.format:
	li	$t0, '\n'		# char lineBreak = '\n'
	sb	$t0, ($s1)		# *outputBuff = lineBreak
	addiu	$s1, $s1, 1		# outputBuff += 1

hexWrite.epi:
	move	$v0, $s1		# return updated &outputBuffer
	lw	$ra, ($sp)
	lw	$s0, 4($sp)
	lw	$s1, 8($sp)
	addiu	$sp, $sp, 32
	jr	$ra			# return from hexWrite function

#--------------------------------------------------
# Sub-Function
# Name: convertByte
# ********************
#  Input Variable: $a0 - int byteEval
# Output Variable: $v0 - char byteEval
#--------------------------------------------------
convertByte:
	bgt	$a0, 0x9, convertByte.high

convertByte.low:
	addiu	$v0, $a0, 0x30		# byteEval += '0'
	jr	$ra

convertByte.high:
	addiu	$a0, $a0, 0x41		# byteEval += 'A'
	subiu	$v0, $a0, 0xA		# byteEval -= 10

convertByte.return:
	jr	$ra

#--------------------------------------------------
# Function
# Name: hexWrite.test
# ********************
#  
# :: Example Runtime Output ::
# mars nc 0x10040000-0x10040010 hexWrite.asm
# U+ABCD
# U+ABCDE
# U+10ABCD
# 
# Mem[0x10040000] 0x42412b55      0x550a4443      0x4342412b      0x550a4544
# Mem[0x10040010] 0x4130312b      0x0a444342      0x00000000      0x00000000
#--------------------------------------------------
hexWrite.test:
	li	$v0, 9
	li	$a0, 100
	syscall
	move	$s0, $v0

	move	$a1, $v0
	li	$a0, 0xABCD
	jal	hexWrite
	
	move	$a1, $v0
	li	$a0, 0xABCDE
	jal	hexWrite

	move	$a1, $v0
	li	$a0, 0x10ABCD
	jal	hexWrite

	move	$a0, $s0
	li	$v0, 4
	syscall

	li	$v0, 10
	syscall

