#--------------------------------------------------
# Function
# Name: getFileNames
# ********************
# (int fdIn, int fdOut) getFileNames(int argc, char *argv[])
#
# Description:
#   Get the file names and return the file descriptors of the open files in the v-registers.
#
# Registers:
#	$s1: file_in name
#	$s2: file_out name
#	$s3: input file descriptor
#	$v0: return input file descriptor
#	$v1: return output file descriptor
#
# Revision History:
#   2012.02.27	Keith Salmon <kswr8@mail.umkc.edu>	getFileNames Function Creation
#--------------------------------------------------
	.globl 	getFileNames 
	.text

getFileNames:
	
	
		addiu	$sp, $sp, -16	# allocate memory on the stack
		sw	$ra, ($sp)	# store return address on stack
		sw	$s1, 4($sp)	# store contents of $s1 on stack
		sw	$s2, 8($sp)	# store contents of $s2 on stack
		sw	$s3, 12($sp)	# store contents of $s3 on stack
		
		lw	$s1, 4($a1)	# load address of input string
		lw	$s2, 8($a1)	# load address of output string	

		#			# Get file descriptors
					
		move	$a0, $s1	# (a0.2 <- s1) getOpenFile input &file_in_name
		li	$a1, 0		# (a1.2 <- 0 )  getOpenFile input 0 (read file)
		jal	getOpenFile	# call getOpenFile(&file_in_name, 0)
		move 	$s3, $v0	# (s3 <- v0) inputFileDesc = getOpenFile(&file_in_name, 0)
			
		move	$a0, $s2	# (a0.2 <- s2) getOpenFile input &file_out_name
		li	$a1, 1		# (a1.2 <- 1 ) getOpenFile input 1 (write file)
		jal	getOpenFile	# call getOpenFile(&file_out_name, 1)

					# Load return arguments for getFileNames()
		move	$v1, $v0	# (v1.1 <- v0.2) return output file descriptor
		move	$v0, $s3	# (v0.1 <-   s3) return input file descriptor
				
		lw	$ra, ($sp)	# load return address from the stack
		lw	$s1, 4($sp)	# load stored $s1 value from stack
		lw	$s2, 8($sp)	# load stored $s2 value from stack
		lw	$s3, 12($sp)	# load stored $s3 value from stack
		addiu	$sp, $sp, 16	# deallocate memory on stack
		jr	$ra		# return from getFileNames
