#--------------------------------------------------
# Function
# Name: getLine
# ********************
# char* getLine(char *inBuffer, char *lineBuffer)
#   cursor1 = inBuffer
#   cursor2 = lineBuffer
#   repeat:
#     c = *cursor1
#     *cursor2 = c
#     cursor1 += 1
#     cursor2 += 1
#   until c == ‘\n’
# return cursor1
#
# Description:
#   Copies one line of input from the input buffer to a lineBuffer and
#   returns an updated pointer to the input buffer.
#
# Register Usage:
#   Function Input Variables (.0: syscall, .1: getLine)
#     $a0: char *inBuffer     (input variable - pointer to input buffer)
#     $a1: char *lineBuffer   (input variable - pointer to line buffer)
#   Function Output Variables (.0: syscall, .1: getLine)
#     $v0: return *inBuffer (return - updated address to inBuffer)
#   Temporary Variables
#     $t0: char *inputBuffer
#     $t1: char *lineBuffer
#     $t9: char inputBufferChar
# 
# Revision History:
#   2012.02.13	Andrew Blackton <aabxvc@mail.umkc.edu>	Created getLine function
#--------------------------------------------------
.globl	getLine
getLine:
	move	$t0, $a0	# (t0 <- a0.1) Initialize Address of inputBuffer
	move	$t1, $a1	# (t1 <- a1.1) Initialize Address of lineBuffer

getLineLoop:
	lbu	$t9, ($t0)	# inputBufferChar = *inputBuffer
	sb	$t9, ($t1)	# *lineBuffer = inputBufferChar
	addi	$t0, $t0, 1	# *inputBuffer++  Increment inputBuffer address by a byte
	addi	$t1, $t1, 1	# *lineBuffer++  Increment lineBuffer address by a byte
	beq	$t9, '\n', getLineExit	# exit function when inputBufferChar = '\n'
	j	getLineLoop

getLineExit:
	move	$v0, $t0	# return(*inputBuffer)
	jr	$ra		# exit getLine()
