#--------------------------------------------------
# encodeTest.asm
# Function
# Name: encodeTest
# ********************
#
# Description:
# Tests the encode function's 1-byte, 2-byte, 3-byte, and 4-byte branches
#   
# Revision History:
#   2012.02.17	Andrew Blackton <aabxvc@mail.umkc.edu>	Encode Test Bench Function
#--------------------------------------------------
	.data
functionOutString:	.asciiz "\nFunction Return: "
postFunctionOutput:	.asciiz "\n:: ---- :: ---- :: ---- ::\n"
giveMeRoom:	.word 0:10
outputBuffer0:	.word 0:1
outputBuffer1:	.word 0:1
outputBuffer2:	.word 0:1
outputBuffer3:	.word 0:1
giveMeRoom1:	.word 0:10
outputBuffer4:	.word 0:10
giveMeRoom2:	.word 0:10
	.text
encodeTest:

	# Test encode1
	# Expected Result	00001111
	# outBuffer Contents	00001111
	la	$s0, outputBuffer0	# initialize *outBuffer
	li	$a0, 0x0F		# initialized int codePoint
	move	$a1, $s0
	jal	encode
	move	$s0, $v0		# outBuffer = encode(codePoint, *outBuffer)

	# Test encode2
	# Expected Result	11000011 10111111
	# outBuffer Contents	11000011 10111111
	la	$s0, outputBuffer1	# initialize *outBuffer
	li	$a0, 0xFF		# initialized int codePoint
	move	$a1, $s0		# 
	jal	encode			# call encode(codePoint, *outBuffer)
	move	$s0, $v0		# outBuffer = encode(codePoint, *outBuffer)

	# Test encode3
	# Expected Result	11101111 10111111 10111111
	# outBuffer Contents	11101111 10111111 10111111 (0x00BFBFEF)
	la	$s0, outputBuffer2	# initialize *outBuffer
	li	$a0, 0xFFFF		# initialized int codePoint
	move	$a1, $s0		# 
	jal	encode			# call encode(codePoint, *outBuffer)
	move	$s0, $v0		# outBuffer = encode(codePoint, *outBuffer)

	# Test encode4
	# Expected Value	11110100 10001111 10111111 10111111
	# outBuffer Contents	11110100 10001111 10111111 10111111
	la	$s0, outputBuffer3	# initialize *outBuffer
	li	$a0, 0x10FFFF		# initialized int codePoint
	move	$a1, $s0		# 
	jal	encode			# call encode(codePoint, *outBuffer)
	move	$s0, $v0		# outBuffer = encode(codePoint, *outBuffer)

	# Cumulative Output
	# Mem[0x10010090] 0xefbfc30f      0x8ff4bfbf      0x0000bfbf      0x00000000
	# (0x0FC3BFEFBFBFF48FBFBF)
	# Expected Value	00001111 1100001110111111 111011111011111110111111 11110100100011111011111110111111
	# outBuffer Contents	00001111 1100001110111111 111011111011111110111111 11110100100011111011111110111111
	# Test encode1
	la	$s0, outputBuffer4	# initialize *outBuffer

	li	$a0, 0x0F		# initialized int codePoint
	move	$a1, $s0
	jal	encode
	move	$s0, $v0		# outBuffer = encode(codePoint, *outBuffer)

	# Test encode2
	li	$a0, 0xFF		# initialized int codePoint
	move	$a1, $s0		# 
	jal	encode			# call encode(codePoint, *outBuffer)
	move	$s0, $v0		# outBuffer = encode(codePoint, *outBuffer)

	# Test encode3
	li	$a0, 0xFFFF		# initialized int codePoint
	move	$a1, $s0		# 
	jal	encode			# call encode(codePoint, *outBuffer)
	move	$s0, $v0		# outBuffer = encode(codePoint, *outBuffer)

	# Test encode4
	li	$a0, 0x10FFFF		# initialized int codePoint
	move	$a1, $s0		# 
	jal	encode			# call encode(codePoint, *outBuffer)
	move	$s0, $v0		# outBuffer = encode(codePoint, *outBuffer)

b	skipSurrogate1
	# Surrogate Code Point Low
	li	$a0, 0xD800		# initialized int codePoint
	move	$a1, $s0		# 
	jal	encode			# call encode(codePoint, *outBuffer)
	move	$s0, $v0		# outBuffer = encode(codePoint, *outBuffer)
skipSurrogate1:

b	skipSurrogate2
	# Surrogate Code Point High
	li	$a0, 0xDFFF		# initialized int codePoint
	move	$a1, $s0		# 
	jal	encode			# call encode(codePoint, *outBuffer)
	move	$s0, $v0		# outBuffer = encode(codePoint, *outBuffer)
skipSurrogate2:

	# Invalid Code Point
	li	$a0, 0xFFFFFFF		# initialized int codePoint
	move	$a1, $s0		# 
	jal	encode			# call encode(codePoint, *outBuffer)
	move	$s0, $v0		# outBuffer = encode(codePoint, *outBuffer)

exit:
	li	$v0, 10			# (v0.0) terminate execution
	syscall				# graceful termination

error:
	li	$v0, 17			# (v0.0) syscall 17 - exit2 terminate w/ value
	li	$a0, 1			# (a0.0) exit w/ error code 1 (error)
	syscall



