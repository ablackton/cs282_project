#--------------------------------------------------
# decode.asm
# Function
# Name: decode
# ********************
# int decode(int inputB0, int inputB1, int inputB2, int inputB3)
# 	if inputB4 != 0:	// 4-byte code
# 		outputB3 =  inputB3 & 0x3F
# 		outputB2 = (inputB2 & 0x3F) << 6
# 		outputB1 = (inputB1 & 0x3F) << 12
# 		outputB0 = (inputB0 & 0x07) << 18
# 	else if inputB3 != 0: // 3-byte code
# 		outputB2 =  inputB2 & 0x3F
# 		outputB1 = (inputB1 & 0x3F) << 6
# 		outputB0 = (inputB0 & 0x0F) << 12
# 	else if inputB1 != 0: // 2-byte code
# 		outputB1 =  inputB1 & 0x3F
# 		outputB0 = (inputB0 & 0x1F) << 6
# 	return outputB0 * outputB1 * outputB2 * outputB3
#
# Description:
# Converts UTF-8 encoded value to hexadecimal.
#
# |   Byte 3  |   Byte 2  |   Byte 1  |   Byte 0  | 
# | 0000 0000 | 0000 0000 | 0000 0000 | 0000 0000 |
#   
# Register Usage:
#   Function Input Variables (.0: syscall, .1: decode)
#     $a0: inputB0
#     $a1: inputB1
#     $a2: inputB2
#     $a3: inputB3
#   Function Output Variables (.0: syscall, .1: decode)
#     $v0: return (outputB3 || outputB2 || outputB1 || outputB0)
#   Temporary Variables
#     $t0: outputB0
#     $t1: outputB1
#     $t2: outputB2
#     $t3: outputB3
# 
# Revision History:
#   2012.03.08	Andrew Blackton <aabxvc@mail.umkc.edu>	Creation
#--------------------------------------------------

.globl	decode
.text

decode:
	# Function Input Variables
	# $a0.1: inputB3
	# $a1.1: inputB2
	# $a2.1: inputB1
	# $a3.1: inputB0
	move	$t0, $0			# initialize outputB0 = 0
	move	$t1, $0			# initialize outputB1 = 0
	move	$t2, $0			# initialize outputB2 = 0
	move	$t3, $0			# initialize outputB3 = 0
		
	bnez	$a3, decode.four	# if inputB3 != 0, then decode four bytes
	bnez	$a2, decode.three	# if inputB2 != 0, then decode three bytes
	bnez	$a1, decode.two		# if inputB1 != 0, then decode two bytes
	bnez	$a0, decode.one		# if inputB0 != 0, then decode one byte

decode.four:
	andi	$t3, $a3, 0x3F		# (t3 <- a3.1) outputB3 = inputB3 && 0x3F
	andi	$t2, $a2, 0x3F		# (t2 <- a2.1) outputB2 = inputB2 && 0x3F
	sll	$t2, $t2, 6		# outputB2 = (inputB2 && 0x3F) << 6
	andi	$t1, $a1, 0x3F		# (t1 <- a1.1) outputB1 = inputB1 && 0x3F
	sll	$t1, $t1, 12		# outputB1 = (inputB1 && 0x3F) << 12
	andi	$t0, $a0, 0x07		# (t0 <- a0.1) outputB0 = inputB0 && 0x7F
	sll	$t0, $t0, 18		# outputB0 = (inputB0 && 0x07) << 18
	j	decode.prepareReturn

decode.three:
	andi	$t2, $a2, 0x3F		# (t2 <- a2.1) outputB2 = inputB2 && 0x3F
	andi	$t1, $a1, 0x3F		# (t1 <- a1.1) outputB1 = inputB1 && 0x3F
	sll	$t1, $t1, 6		# outputB1 = (inputB1 && 0x3F) << 6
	andi	$t0, $a0, 0x0F		# (t0 <- a0.1) outputB0 = inputB0 && 0x7F
	sll	$t0, $t0, 12		# outputB0 = (inputB0 && 0x0F) << 12
	j	decode.prepareReturn

decode.two:
	andi	$t1, $a1, 0x3F		# (t1 <- a1.1) outputB1 = inputB1 && 0x3F
	andi	$t0, $a0, 0x1F		# (t0 <- a0.1) outputB0 = inputB0 && 0x1F
	sll	$t0, $t0, 6		# outputB0 = (inputB0 && 0x1F) << 6
	j	decode.prepareReturn
	
decode.one:
	move	$v0, $a0		# (v0.1 <- a0.1) return(inputB0)
	jr	$ra			# return from decode

decode.prepareReturn:
	or	$v0, $t3, $t2		# (v0.1) Prepare return values outputB3 || outputB2
	or	$v0, $v0, $t1		# (v0.1) Prepare return values outputB3 || outputB2 || outputB1
	or	$v0, $v0, $t0		# (v0.1) Prepare return values outputB3 || outputB2 || outputB1 || outputB0
	jr	$ra			# return from decode
