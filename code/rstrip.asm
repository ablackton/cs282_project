#--------------------------------------------------
# Function
# Name: rstrip
# ********************
# void rstrip(char *buffer) {
#   cursor = buffer
#   while *cursor != ‘\0’:
#     cursor += 1
#     cursor -= 1
#   while *cursor = ‘\n’ or ‘\r’ or ‘\t’ or ‘ ‘:
#     *cursor = ‘\0’
#     cursor -= 1
# }
#
# Description:
#   Strip the trailing '\r\n' or '\n' from the line buffer.
# 
# Register Usage:
#	$a0 = *buffer
#	$t0 = cursor
#	$t1 = *cursor
#	$t2 = first ascii non-whitespace character
#
# Revision History:
#   2012.02.27	Lee Carpenter <c6fd@mail.umkc.edu>	rstrip function completion
#--------------------------------------------------
# rstrip
   
    
	.globl rstrip
	.text
rstrip:
	move	$t1, $a0		# cursor = *buffer

loop:
	addiu	$t1, $t1, 1 		# cursor++
	lbu	$t0, ($t1)		# Load the current character into $t0 according to $t1
	bne 	$t0, $0, loop		# while cursor != whitespace char

jump:

	subiu	$t1, $t1, 1 		# cursor--
	lbu	$t0, ($t1)		# Load the current character into $t1 for compare
	beq	$t0, 0xA, setnull	# *cursor = '\n' therefore load null into address
	beq	$t0, 0xD, setnull	# *cursor = '\r' therefore load null into address
	beq	$t0, 0x9, setnull	# *cursor = '\t' therefore load null into address
	beq	$t0, 0x20, setnull	# *cursor = ' '  therefore load null into address
	b	end			# jump to end
	
setnull:	
	sb 	$0, ($t1)		# Store null into the whitespace character
	b	jump			# loop to jump
		
end:	jr	$ra			# return from the function
