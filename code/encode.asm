#--------------------------------------------------
# encode.asm
# Function
# Name: encode
# ********************
# char* encode(int codePoint, char *outBuffer)
#
# Description:
# Writes a given hex-encoded codepoint to the outBuffer; 
# then returns the updated outBuffer address.
#   
# Register Usage:
#   Function Input Variables (.0: syscall, .1: encode)
#     $a0: int codePoint
#     $a1: char *outBuffer   (input variable - pointer to output buffer)
#   Function Output Variables (.0: syscall, .1: encode)
#     $v0: return *outBuffer (return - updated pointer to output buffer)
#   Temporary Variables
#     $t0: int codePoint
#     $t1: char *outBuffer
#     $t2: int codePointB0
#     $t3: int codePointB1
#     $t4: int codePointB2
#     $t5: int codePointB3
#     $t8: int encodedValueSize
#     $t9: int encodedValue
# 
# Revision History:
#   2012.02.06	Andrew Blackton <aabxvc@mail.umkc.edu>	Origional encode function
#--------------------------------------------------
.globl encode
.text
encode:
	move	$t0, $a0		# (t0 <- a0.1) Move input variable int codePoint
	move	$t1, $a1		# (t1 <- a1.1) Move input variable char *outBuffer
	
	blt	$t0, 0x80, encode1	# if(codePoint<0x80) encode single byte
	blt	$t0, 0x800, encode2	# if(codePoint<0x800) encode two bytes
	blt	$t0, 0x10000, encode3	# if(codePoint<0x10000) encode three bytes
	blt	$t0, 0x110000, encode4	# if(codePoint<0x110000) encode four bytes
	b	encodeAbort		# invalid input codePoint

encode1:
	andi	$t2, $t0, 0x7F		# encodedValue = codePoint && 0x7F
					# Single Byte Mask 
					# 0x7F = 0111 1111
	sb	$t2, ($t1)		# *outBuffer=encodedValue
					# write single byte to output buffer
	addiu	$t1, $t1, 1		# &outBuffer++ increment output buffer address
	move	$v0, $t1		# return char *outBuffer (updated &outBuffer)
	jr	$ra			# return from encode

encode2:
	srl	$t2, $t0, 6		# codePointB0 = codePoint>>6
	andi	$t2, $t2, 0x1F		# codePointB0 = codePointB0 && 0x1F
	ori	$t2, $t2, 0xC0		# codePointB0 = codePointB0 && 0xC0
	sb	$t2, ($t1)		# outBuffer = codePointB0

	andi	$t3, $t0, 0x3F		# codePointB1 = codePoint && 0x3F
	ori	$t3, $t3, 0x80		# encodedValue = codePointB1 || 0x80
	sb	$t3, 1($t1)		# (outBuffer+1) = codePointB1

	addiu	$t1, $t1, 2		# (&outBuffer+2) increment output buffer address
	move	$v0, $t1		# return char *outBuffer (updated (*outBuffer)
	jr	$ra			# return from encode

encode3:
	# the code points U+D800 through U+DFFF must never be encoded with UTF-8
	blt	$t0, 0xD800, encode3Cont # Surrogate Code Point Range U+D800 - U+DFFF
	bgt	$t0, 0xDFFF, encode3Cont # Surrogate Code Point Range U+D800 - U+DFFF
	b	encodeAbort

encode3Cont:
	srl	$t2, $t0, 12		# codePointB0 = codePoint>>12
	andi	$t2, $t2, 0xF		# codePointB0 = codePointB0 && 0x0F
	ori	$t2, $t2, 0xE0		# codePointB0 = codePointB0 || 0xE0
	sb	$t2, ($t1)		# outBuffer = codePointB0

	srl	$t3, $t0, 6		# codePointB1 = codePoint>>6
	andi	$t3, $t3, 0x3F		# codePointB1 = codePointB1 && 0x3F
	ori	$t3, $t3, 0x80		# codePointB1 = codePointB1 || 0x3F
	sb	$t3, 1($t1)		# outBuffer = codePointB1

	andi	$t4, $t0, 0x3F		# codePointB2 = codePoint && 0x3F
	ori	$t4, $t4, 0x80		# codePointB2 = codePoint || 0x80
	sb	$t4, 2($t1)		# (outBuffer+2) = codePointB1

	addiu	$t1, $t1, 3		# outBuffer += 3 - increment output buffer address
	move	$v0, $t1		# return &outBuffer (updated &outBuffer))
	jr	$ra			# return from encode

encode4:
	srl	$t2, $t0, 18		# codePointB0 = codePoint>>18
	andi	$t2, $t2, 0x7		# codePointB0 = codePointB0 && 0x7
	ori	$t2, $t2, 0xF0		# codePointB0 = codePointB0 || 0xF0
	sb	$t2, ($t1)		# outBuffer = codePointB0

	srl	$t3, $t0, 12		# codePointB1 = codePoint>>12
	andi	$t3, $t3, 0x3F		# codePointB1 = codePointB1 && 0x3F
	ori	$t3, $t3, 0x80		# codePointB1 = codePointB1 || 0x80
	sb	$t3, 1($t1)		# (outBuffer+1) = codePointB1

	srl	$t4, $t0, 6		# codePointB2 = codePoint>>6
	andi	$t4, $t4, 0x3F		# codePointB2 = codePointB2 && 0x3F
	ori	$t4, $t4, 0x80		# codePointB2 = codePointB2 || 0x80
	sb	$t4, 2($t1)		# (outBuffer+2) = codePointB2

	andi	$t5, $t0, 0x3F		# codePointB3 = codePoint && 0x3F
	ori	$t5, $t5, 0x80		# codePointB3 = codePointB3 || 0x80
	sb	$t5, 3($t1)		# (outBuffer+3) = codePointB3

	addiu	$t1, $t1, 4		# outBuffer++ increment output buffer address
	move	$v0, $t1		# return &outBuffer (updated &outBuffer)
	jr	$ra			# return from encode

.data
encodePrintFunctionNameError:	.asciiz ":: ---- :: encode :: ---- :: error :: ---- ::\n"
encodePrintInvalidCodePoint:	.asciiz "::     Invalid Code Point Input Variable   ::\n"
encodePrintSurrogateCodePoint:	.asciiz "::   Surrogate Code Point Input Variable   ::\n"
.text
encodeAbort:
	la	$a0, encodePrintFunctionNameError
	li	$v0, 4
	syscall

	la	$a0, encodePrintInvalidCodePoint
	li	$v0, 4
	syscall

	la	$a0, encodePrintFunctionNameError
	li	$v0, 4
	syscall

	li	$v0, 17			# (v0.0) syscall 17 - exit2 terminate w/ value
	li	$a0, 1			# (a0.0) exit w/ error code 1 (error)
	syscall
