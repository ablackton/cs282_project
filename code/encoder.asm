#--------------------------------------------------
# encoder.asm
# Function
# Name: encoder
# ********************
# char* encoder(char *inBuffer, char* end, char* outBuffer)
#   while inBuffer < end
#   inBuffer = getLine(inBuffer, lineBuffer)
#   rstrip(lineBuffer)
#   value = getHexNum(lineBuffer)
#   outBuffer = encode(value, outBuffer)
#   inBuffer++
# return outBuffer
#
# Description:
#   Takes the codepoints held in the input buffer line by line, and writes the encoded 
#   code point held in each line to the output buffer.
# 
# Register Usage:
#   Function Input Variables (.0: syscall, .1: encoder)
#     $a0: char *inBuffer
#     $a1: char *end
#     $a2: char *outBuffer
#   Function Output Variables (.0: syscall, .1: encoder)
#     $v0: char outBuffer
#   Temporary Registers
#     $t0: getHexNumValue
#   Saved Variables
#     $s0: inBuffer
#     $s1: outBuffer
#     $s2: lineBuffer
#     $s6: end
#
# Revision History:
#   2012.02.16	Andrew Blackton <aabxvc@mail.umkc.edu>	Wrote function encoder
#--------------------------------------------------
.globl	encoder

.text
encoder:
	addiu	$sp, $sp, -32		# Allocate Space on stack for procedure calls
	sw	$ra, 0($sp)		# Save $ra to stack
	sw	$s0, 4($sp)
	sw	$s1, 8($sp)
	sw	$s2, 16($sp)
	sw	$s3, 20($sp)
	sw	$s6, 24($sp)

	move	$s0, $a0		# (s0 <- a0) save input buffer location
	move	$s6, $a1		# (s6 <- a1) save address end of input buffer
	move	$s1, $a2		# (s1 <- a2) save output buffer location

	# syscall 9 - sbrk (allocate heap memory)
	# $a0 = number of bytes to allocate
	# Return $v0 contains address of allocated memory
	li	$a0, 0xF		# Allocate 15 (0xF) bytes
	li	$v0, 9			# syscall 9 - sbrk (allocate heap memory)
	syscall				# Allocate space on heap for inputBuffer
	move	$s2, $v0		# initialize &linBuffer

encode_while:
	bge	$s0, $s6, encoderDone	# if inBuffer >= end then return updated outBuffer
	move	$a0, $s0		# initialize getLine's first input variable
	move	$a1, $s2		# initialize getLine's second input variable
	jal	getLine			# call getLine(&inBuffer, &lineBuffer)
	move	$s0, $v0		# update input buffer's address returned from getLine

	move	$a0, $s2		# &lineBuffer initialize rstrip's input variables
	jal	rstrip			# call rstrip(&lineBuffer)

	move	$a0, $s2		# &lineBuffer initialize getHexNum's input variables
	jal	getHexNum		# call getHexNum(&lineBuffer)
	move	$t0, $v0		# getHexNumValue = getHexNum(&lineBuffer)

	move	$a0, $t0		# getHexNumValue - initialize encode's input variables
	move	$a1, $s1		# &outBuffer - initialize encode's input variables
	jal	encode			# call encode(getHexNumValue, &outBuffer)
	move	$s1, $v0		# outBuffer = encode(getHexNumValue, &outBuffer)
	b	encode_while
	
encoderDone:
	move	$v0, $s1		# return outBuffer
	lw	$ra, 0($sp)		# Restore $ra from stack
	lw	$s0, 4($sp)
	lw	$s1, 8($sp)
	lw	$s2, 16($sp)
	lw	$s3, 20($sp)
	lw	$s6, 24($sp)
	addiu	$sp, $sp, 32		# Return space to stack
	jr	$ra

