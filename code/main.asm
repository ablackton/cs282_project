#--------------------------------------------------
# main.asm
# Driver Function
# Name: main
# ********************
#
# Input Parameters:
#   * a processing code (e|d|n)
#   * input file name
#   * output file name
#
# * Input Parameters from command line
# * Allocate an input buffer 65,536 (0x10000) bytes
# * Allocate an output buffer 65,536 (0x10000) bytes
# * Call Function: getFileNames
# * Read the input file into the input buffer 
#   - variable needed from syscall 14: number of bytes read
#     * Allows you determine if there was an I/O error
#     * Allows you to find the end of the input buffer
# * Close input file (file descriptor)
# * call function: encoder
# * Compute how many bytets to write to the output file
# * Write the output buffer to the output file 
# * Check for I/O errors and close output file
# 
# NOTE: It's crucial that you write your error message to the console, using syscall 4.
# 
# Register Usage:
#   Function Input Variables (.0: syscall, .1: main, .2 getFileNames, .3 encoder)
#     $a0: int argc
#     $a1: char *argv[]
#   Function Output Variables (.0: syscall, .2: getFileNames, .3 encoder)
#     $v0:
#       .0:
#       .2: int fdInputFile
#     $v1: return fdOutputFile
#   Saved Variables
#     $s0: int argc
#     $s1: char *argv[3]
#     $s2: int fdInputFile
#     $s3: int fdOutputFile
#     $s4: &endInputBuffer
#     $s5: &inputBuffer
#     $s6: &endOutputBuffer
#     $s7: &outputBuffer
#
# Revision History:
#   2012.02.22	Andrew Blackton <aabxvc@mail.umkc.edu>	Main Driver Function Written
#--------------------------------------------------
	.data
space0:	.word	0:1
inputBuffer:	.space 65536		# Allocate an input  buffer 65,536 (0x10000) bytes
space1:	.word	0:1
outputBuffer:	.space 65536		# Allocate an output buffer 65,536 (0x10000) bytes

	.globl	main

	.text
main:
	move	$s0, $a0		# (s0 <- a0.1) # save int argc
					# number of program input arguments
	move	$s1, $a1		# (s1 <- a1.1) # save *argv[]
					# address of programs input arguments exit:
	blt	$s0, 0x3, exitInsufficientArguments	# if argc != 3 then exit with error
	lbu	$t0, ($s1)

	# Call Fuction: getFileNames
	# Arguments:
	#	$a0: int argc
	#	$a1: &argv
	move	$a0, $s0		# (a0.2) initialize getFileName's first input variable
	move	$a1, $s1		# (a1.2) initialize getFileName's second input variable
	jal	getFileNames		# call getFileNames(argc, &argv)
	move	$s2, $v0		# (s2 <- v0.2) save input file's file descriptor
	move	$s3, $v1		# (s3 <- v1.2) save output file's file descriptor

	# syscall 9 - sbrk (allocate heap memory)
	# $a0 = number of bytes to allocate
	# Return $v0 contains address of allocated memory
	li	$a0, 65536		# Allocate 65,536 (0x10000) bytes
	li	$v0, 9			# syscall 9 - sbrk (allocate heap memory)
	syscall				# Allocate space on heap for inputBuffer
	move	$s5, $v0		# initialize &inputBuffer

	# syscall 9 - sbrk (allocate heap memory)
	# $a0 = number of bytes to allocate
	# Return $v0 contains address of allocated memory
	li	$a0, 65536		# Allocate 65,536 (0x10000) bytes
	li	$v0, 9			# syscall 9 - sbrk (allocate heap memory)
	syscall				# Allocate space on heap for outputBuffer
	move	$s7, $v0		# initialize &outputBuffer


	# Read the input file into the input buffer
	# syscall 14 (read from file)
	# Arguments:
	#	$a0: file descriptor of file to be read
	#	$a1: address of input buffer
	#	$a2: maximum number of characters to read (0x10000)
	# Returns:
	#	$v0: contains number of characters read (0 if end-of-file, negative if error)
	li	$v0, 14			# (v0.0) syscall 14 - read from file
	move	$a0, $s2		# (a0.0 <- s2) initialize file descriptor of file to be read
	move	$a1, $s5		# (a1.0) initialize destination for input file's contents
	li	$a2, 0x10000		# (a2.0) maxiumum number of characters to read from input file
	syscall				# read from file

	move	$t0, $v0		# (t0 <- v0.0) read from file return value
	bltz	$t0, errorReadingFile	# if return value < 0 error encountered then exit
	beqz	$t0, errorNoInputFile	# if return value == 0 error encountered then exit
	addu	$s4, $s5, $t0		# find address - end of inputBuffer
	
	# Close Input File
	li	$v0, 16			# (v0.0) syscall 16 - close file
	move	$a0, $s2		# (a0.0 <- s2) initialize file descriptor of file to be closed
	syscall				# close inputFile handle

	lw	$t0, ($s1)		# check processing code (e|d|n)
	lb	$t0, ($t0)		# check processing code (e|d|n)
	beq	$t0, 0x65, drive.encode
	beq	$t0, 0x64, drive.decode
	beq	$t0, 0x6E, drive.decode_noerror
	b	exitUnknownProcessingCode

drive.encode:
	# Call function: encoder
	# Arguments:
	#	$a0: &inBuffer
	#	$a1: &endInputBuffer
	#	$a2: &outBuffer
	# Returns:
	#	$v0: &outBuffer - end
	move	$a0, $s5		# (a0.3 <- s5) initialize encoder's arguments &inputBuffer
	move	$a1, $s4		# (a1.3 <- s4) initialize encoder's arguments &endInputBuffer
	move	$a2, $s7		# (a2.3 <- s7) initialize encoder's arguments &ouputBuffer
	jal	encoder			# call encoder(&inputBuffer, &endInputBuffer, &outputBuffer)
	move	$s6, $v0		# *endOutputBuffer = return(outputBuffer)
	b	drive.writeOutputBuffer

drive.decode_noerror:
	# Call function: decoder
	# Arguments:
	#	$a0: &inBuffer
	#	$a1: &endInputBuffer
	#	$a2: &outBuffer
	# Returns:
	#	$v0: &outBuffer - end
	move	$a0, $s5		# (a0.4 <- s5) initialize decoder's arguments &inputBuffer
	move	$a1, $s4		# (a1.4 <- s4) initialize decoder's arguments &endInputBuffer
	move	$a2, $s7		# (a2.4 <- s7) initialize decoder's arguments &ouputBuffer
	jal	decoder			# call decoder(&inputBuffer, &endInputBuffer, &outputBuffer)
	move	$s6, $v0		# *endOutputBuffer = return(outputBuffer)
	b	drive.writeOutputBuffer

drive.decode:
# TODO: Implement decode with errors

drive.writeOutputBuffer:
	# Write the output buffer's contents to the output file
	# syscall 15 - write to file
	# Arguments:
	#	$a0: file descriptor of file to be written
	#	$a1: address of output buffer
	#	$a2: number of characters to write
	# Returns:
	#	$v0: contains number of characters written (negative if error)
	move	$a0, $s3		# (a0.0 <- s3) file descriptor of file to be written fdOutputFile
	move	$a1, $s7		# (a1.0 <- s3) address of output buffer
	subu	$a2, $s6, $s7		# number of characters to write = &endOutputBuffer - &outputBuffer
	li	$v0, 15			# syscall 15 - write to file
	syscall				# write to file
	bltz	$v0, errorWritingToFile	# if return is negative then error writing to file

	# Close Output File
	li	$v0, 16			# (v0.0) syscall 16 - close file
	move	$a0, $s3		# (a0.0 <- s3) initialize file descriptor of file to be closed
	syscall				# close inputFile handle

.data
s.mainFinished:	.asciiz ":: ---- :: ---- :: main - encode :: ---- :: ---- ::\n::     Execution Completed - Examine Output      ::\n:: ---- :: ---- :: main - encode :: ---- :: ---- ::\n"
.text
	#li	$v0, 4			# (v0.0) print string
	#la	$a0, s.mainFinished
	#syscall

exit:
	li	$v0, 10			# (v0.0) terminate execution
	syscall				# graceful termination

	.data
mainErrorFuncName:	.asciiz	"\n:: ---- :: Error :: ---- :: ---- :: main :: ---- ::\n"
mainErrorReadingFile:	.asciiz "::  Error encountered while reading input file   ::"
mainErrorWritingFile:	.asciiz "::  Error encountered while writing output file  ::"
mainErrorNullInput:	.asciiz "::  Input File is Empty                          ::"
mainInsufficientArguments: .asciiz ":: Insufficient Number of Command Line Arguments ::\n:: Required Input Parameters:\n:: processing code , inputFileName, outputFileName"
mainUnknownProcessingCode: .asciiz ":: Unknown Processing Code (decode, encode, none)::"
	.text
# main Error Handling - will exit w/ error after entering exception handler
# Load Error Message String
# Register Usage:
#	$t0: Address of Error Message to print
exitInsufficientArguments:
	la	$t0, mainInsufficientArguments
	b	mainPrintError

exitUnknownProcessingCode:
	la	$t0, mainUnknownProcessingCode
	b	mainPrintError

errorReadingFile:
	la	$t0, mainErrorReadingFile	# (t0.1) Load Error Message Address
	b	mainPrintError

errorNoInputFile:
	la	$t0, mainErrorNullInput		# (t0.1) Load Error Message Address
	b	mainPrintError

errorWritingToFile:
	la	$t0, mainErrorWritingFile		# (t0.1) Load Error Message Address
	b	mainPrintError

mainPrintError:
	li	$v0, 4				# (v0.0) print string
	la	$a0, mainErrorFuncName		# (a0.0 <- t0) address of string to print
	syscall					# print null-terminated string

	move	$a0, $t0			# (a0.0) move address of error number to print
	li	$v0, 4				# (v0.0) print string
	syscall

	li	$v0, 4				# (v0.0) print string
	la	$a0, mainErrorFuncName		# (a0.0 <- t0) address of string to print
	syscall					# print null-terminated string
	b	error

error:
	li	$v0, 17			# (v0.0) syscall 17 - exit2 terminate w/ value
	li	$a0, 1			# (a0.0) exit w/ error code 1 (error)
	syscall
